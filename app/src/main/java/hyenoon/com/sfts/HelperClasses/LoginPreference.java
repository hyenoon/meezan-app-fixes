package hyenoon.com.sfts.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import hyenoon.com.sfts.Constants.Constants;

/**
 * Created by us on 3/1/2018.
 */

public class LoginPreference {

    public static boolean getLoginStatus(@NonNull Context context) {
        SharedPreferences preferences = context
                .getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(Constants.LOGIN_KEY, false);
    }

    public static void setLoginStatus(@NonNull Context context, boolean status) {
        SharedPreferences.Editor editor = context

                .getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE)
                .edit();
        editor.putBoolean(Constants.LOGIN_KEY, status).apply();
    }

}
