package hyenoon.com.sfts.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.Area;
import hyenoon.com.sfts.Entities.BodyLOV;
import hyenoon.com.sfts.Entities.CheckInModule;
import hyenoon.com.sfts.Entities.Doctor;
import hyenoon.com.sfts.Entities.DoctorCheckInItem;
import hyenoon.com.sfts.Entities.LOV;
import hyenoon.com.sfts.Entities.ProductLOV;
import hyenoon.com.sfts.Entities.Speciality;
import hyenoon.com.sfts.Entities.Survey;
import hyenoon.com.sfts.Entities.TeamCustomers;
import hyenoon.com.sfts.Entities.UserLocation;
import hyenoon.com.sfts.Entities.Users;
import hyenoon.com.sfts.ServiceModel.GsonFactory;
import hyenoon.com.sfts.Utils.Utils;

public class BasePreferenceHelper extends PreferenceHelper {

    protected static final String KEY_USER = "user";
    protected static final String KEY_USER_SHIFT_STATUS = "user_shift_status";
    protected static final String KEY_DOCTOR_LIST = "doctor_list";
    protected static final String KEY_TEAM_LIST = "doctor_list";

    protected static final String KEY_SPECIALITY_LIST = "speciality_list";
    protected static final String KEY_AREA_CODE_LIST = "area_code_list";
    protected static final String KEY_ADD_DOCTOR_LIST = "add_doctor_list";
    protected static final String KEY_UPDATE_DOCTOR_LIST = "update_doctor_list";
    protected static final String KEY_CHECK_IN_DOCTOR_LIST = "check_in_doctor_list";
    protected static final String KEY_SAVED_CHECK_IN_DOCTOR_LIST = "saved_check_in_doctor_list";
    protected static final String KEY_SAVED_CHECK_IN_FORM_LIST = "saved_check_in_form_list";

    protected static final String KEY_USER_LOCATION_LIST = "user_location_list";
    protected static final String KEY_SURVEY = "survey";
    protected static final String KEY_MISSED_SYNC_CALLS = "missed_sync_call";
    protected static final String KEY_MISSED_CHECKIN_CALLS = "missed_checkin_calls";
    protected static final String KEY_LOCATION_ZERO = "location_zero";
    private static final String FILENAME = "preferences";
    private static final String KEY_LOV_LIST = "lovList";
    private static final String KEY_CUSTOMER_LIST = "customer_list";
    private static final String KEY_CURRENT_LOCATION = "current_location";
    private Context context;

    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public void putNoOfZeroLocationFound(int num) {
        putIntegerPreference(context, FILENAME, KEY_LOCATION_ZERO, num);
    }

    public int getNoOfZeroLocationFound() {

        return getIntegerPreference(context, FILENAME, KEY_LOCATION_ZERO);
    }

    public void putMissedCheckinCalls(int num) {

        putIntegerPreference(context, FILENAME, KEY_MISSED_CHECKIN_CALLS, num);

    }

    public int getMissedCheckinCalls() {

        return getIntegerPreference(context, FILENAME, KEY_MISSED_CHECKIN_CALLS);
    }

    public void putMissedSynCalls(int num) {
        putIntegerPreference(context, FILENAME, KEY_MISSED_SYNC_CALLS, num);
    }

    public int getMissedSynCalls() {

        return getIntegerPreference(context, FILENAME, KEY_MISSED_SYNC_CALLS);
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    private String getJsonDoctorsList(List<Doctor> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    private String getJsonTeamCustomersList(List<TeamCustomers> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    private String getJsonSpecialityList(List<Speciality> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    private String getJsonAreaCodeList(List<Area> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    private String getJsonCheckInDoctorList(List<DoctorCheckInItem> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    private String getJsonUserLocationList(List<UserLocation> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    private ArrayList<Doctor> getDoctorListFromJson(String json) {
        Type collectionType = new TypeToken<ArrayList<Doctor>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    private ArrayList<TeamCustomers> getTeamCustomerListFromJson(String json) {
        Type collectionType = new TypeToken<ArrayList<TeamCustomers>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    private ArrayList<Speciality> getSpecialityListFromJson(String json) {
        Type collectionType = new TypeToken<ArrayList<Speciality>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    private ArrayList<Area> getAreaCodeListFromJson(String json) {
        Type collectionType = new TypeToken<ArrayList<Area>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    private ArrayList<DoctorCheckInItem> getDoctorCheckInListFromJson(String json) {
        Type collectionType = new TypeToken<ArrayList<DoctorCheckInItem>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    private ArrayList<UserLocation> getUserLocationListFromJson(String json) {
        Type collectionType = new TypeToken<ArrayList<UserLocation>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    public void putUser(Users user) {
//        putStringPreference(context, FILENAME, KEY_USER, new GsonBuilder().create().toJson(user));
        putStringPreference(context, FILENAME, KEY_USER, new Gson().toJson(user));
    }

    public Users getUser() {
//        return new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_USER), Users.class);
        return new Gson().fromJson(getStringPreference(context, FILENAME, KEY_USER), Users.class);
    }

    public void putShiftStatus(Boolean status) {
        putBooleanPreference(context, FILENAME, KEY_USER_SHIFT_STATUS, status);
    }

    public boolean getShiftStatus() {
        return getBooleanPreference(context, FILENAME, KEY_USER_SHIFT_STATUS);
    }

    public void putDoctorsList(List<Doctor> data) {
        putStringPreference(context, FILENAME, KEY_DOCTOR_LIST, getJsonDoctorsList(data));
    }

    public void putTeamCustomerList(List<TeamCustomers> data) {
        putStringPreference(context, FILENAME, KEY_TEAM_LIST, getJsonTeamCustomersList(data));
    }


    public void putLOV(List<ProductLOV> data) {
        putStringPreference(context, FILENAME, KEY_LOV_LIST, getJsonLOV(data));
    }

    private String getJsonLOV(List<ProductLOV> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }


    public List<ProductLOV> getLOV() {
        String json = getStringPreference(context, FILENAME, KEY_LOV_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getLOVLIST(json);
    }

    private List<ProductLOV> getLOVLIST(String json) {
        Type collectionType = new TypeToken<List<ProductLOV>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    public ArrayList<Doctor> getDoctorsList() {
        String json = getStringPreference(context, FILENAME, KEY_DOCTOR_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getDoctorListFromJson(json);
    }

    public ArrayList<Doctor> getCustomersList() {
        String json = getStringPreference(context, FILENAME, KEY_CUSTOMER_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getDoctorListFromJson(json);
    }

    public void putCustomerList(List<Doctor> data) {
        putStringPreference(context, FILENAME, KEY_CUSTOMER_LIST, getJsonDoctorsList(data));
    }


    public ArrayList<TeamCustomers> getTeamCustomersList() {
        String json = getStringPreference(context, FILENAME, KEY_TEAM_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getTeamCustomerListFromJson(json);
    }

    public void putSpecialityList(List<Speciality> data) {
        putStringPreference(context, FILENAME, KEY_SPECIALITY_LIST, getJsonSpecialityList(data));
    }

    public ArrayList<Speciality> getSpecialityList() {
        String json = getStringPreference(context, FILENAME, KEY_SPECIALITY_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getSpecialityListFromJson(json);
    }

    public void putAreaCodeList(List<Area> data) {
        putStringPreference(context, FILENAME, KEY_AREA_CODE_LIST, getJsonAreaCodeList(data));
    }

    public ArrayList<Area> getAreaCodeList() {
        String json = getStringPreference(context, FILENAME, KEY_AREA_CODE_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getAreaCodeListFromJson(json);
    }

    public void putSurveyDetails(Survey data) {
        putStringPreference(context, FILENAME, KEY_SURVEY, new GsonBuilder().create().toJson(data));
    }

    public Survey getSurveyDetails() {
        return new GsonBuilder().create().fromJson(getStringPreference(context, FILENAME, KEY_SURVEY), Survey.class);
    }

    public ArrayList<Doctor> getAddedDoctorsList() {
        String json = getStringPreference(context, FILENAME, KEY_ADD_DOCTOR_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getDoctorListFromJson(json);
    }

    public void putAddDoctorList(List<Doctor> data) {
        putStringPreference(context, FILENAME, KEY_ADD_DOCTOR_LIST, getJsonDoctorsList(data));
    }

    public ArrayList<Doctor> getUpdatedDoctorsList() {
        String json = getStringPreference(context, FILENAME, KEY_UPDATE_DOCTOR_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getDoctorListFromJson(json);
    }

    public void putUpdateDoctorList(List<Doctor> data) {
        putStringPreference(context, FILENAME, KEY_UPDATE_DOCTOR_LIST, getJsonDoctorsList(data));
    }

    public void putDoctorCheckInList(List<DoctorCheckInItem> data) {
        putStringPreference(context, FILENAME, KEY_CHECK_IN_DOCTOR_LIST, getJsonCheckInDoctorList(data));
    }

    public ArrayList<DoctorCheckInItem> getDoctorCheckInList() {
        String json = getStringPreference(context, FILENAME, KEY_CHECK_IN_DOCTOR_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getDoctorCheckInListFromJson(json);
    }

    public void putSavedDoctorCheckInList(List<DoctorCheckInItem> data) {
        putStringPreference(context, FILENAME, KEY_SAVED_CHECK_IN_DOCTOR_LIST, getJsonCheckInDoctorList(data));
    }

    public void putSavedSurveyFormCheckInList(JSONArray jsonArray) {
        putStringPreference(context, FILENAME, KEY_SAVED_CHECK_IN_FORM_LIST, jsonArray.toString());
    }

    private String getJsonSurveyFormCheckInList(JSONArray data) {
        return GsonFactory.getConfiguredGson().toJson(data);
//        return new Gson().toJson(data);
    }

    public JSONArray getSavedSurveryFormCheckInList() {
        String json = getStringPreference(context, FILENAME, KEY_SAVED_CHECK_IN_FORM_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getSurveyFormCheckInListFromJson(json);
    }

    private JSONArray getSurveyFormCheckInListFromJson(String json) {
        try {
            return new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
//                Type collectionType = new TypeToken<JSONArray>() {
//        }.getType();
//return  new Gson().fromJson(json,collectionType);
//        Type collectionType = new TypeToken<JSONArray>() {
//        }.getType();
//        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }


    public ArrayList<DoctorCheckInItem> getSavedDoctorCheckInList() {
        String json = getStringPreference(context, FILENAME, KEY_SAVED_CHECK_IN_DOCTOR_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getDoctorCheckInListFromJson(json);
    }

    public void putUserLocationList(List<UserLocation> data) {

        putStringPreference(context, FILENAME, KEY_USER_LOCATION_LIST, getJsonUserLocationList(data));
    }

    public ArrayList<UserLocation> getUserLocationList() {
        String json = getStringPreference(context, FILENAME, KEY_USER_LOCATION_LIST);
        if (Utils.isEmptyOrNull(json))
            return null;

        return getUserLocationListFromJson(json);
    }

    public void clearPref(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME,
                Activity.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public void putCurrentLocation(Context context, double latitude, double longitude) {

        SharedPreferences preferences = context.getSharedPreferences(FILENAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Set<String> setLocation = new HashSet<>();
        setLocation.add(String.valueOf(latitude));
        setLocation.add(String.valueOf(longitude));
        editor.putStringSet(KEY_CURRENT_LOCATION, setLocation);
        editor.apply();
    }

    public Set<String> getCurrentLocation(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(FILENAME,
                Activity.MODE_PRIVATE);
        return preferences.getStringSet(KEY_CURRENT_LOCATION, null);
    }

    public void removeCheckInFormPreference() {
        SharedPreferences preferences = context.getSharedPreferences(FILENAME,
                Activity.MODE_PRIVATE);


        preferences.edit().remove(Constants.KEY_SAVED_CHECK_IN_FORM_LIST).apply();

    }

}

