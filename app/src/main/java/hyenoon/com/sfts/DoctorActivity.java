package hyenoon.com.sfts;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import hyenoon.com.sfts.Entities.Doctor;
import hyenoon.com.sfts.Fragments.AddDoctor;
import hyenoon.com.sfts.Fragments.DoctorCheckIn;
import hyenoon.com.sfts.Fragments.DoctorDetails;
import hyenoon.com.sfts.Fragments.DoctorList;

public class DoctorActivity extends BaseActivity implements
        DoctorList.OnDoctorListFragmentListener,
        DoctorDetails.DoctorDetailsFragmentListener {
    private Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("My Customers");
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_layout, DoctorList.newInstance(), "doctorList")
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.actionbarmenu, menu);

//        int badgeCount = SharedPreference.getInstance(this).getSyncRequestCount();
//        itemSync = menu.findItem(R.id.item_samplebadge);
//        ActionItemBadge.update(this, itemSync, FontAwesome.Icon.faw_refresh, ActionItemBadge.BadgeStyles.RED, badgeCount);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDoctorSelected(Doctor doctor) {
        DoctorDetails doctorDetailsFragment = DoctorDetails.newInstance(doctor);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_layout, doctorDetailsFragment, "DoctorDetails")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onAddDoctor() {
        AddDoctor addDoctorFragment = AddDoctor.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_layout, addDoctorFragment, "AddDoctorFragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDoctorCheckIn(Doctor doctor) {
        Log.d("DoctorActivity", "Doctor check-in successfully");

        DoctorCheckIn doctorCheckIn = DoctorCheckIn.newInstance(doctor);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_layout, doctorCheckIn, "DoctorCheckInItem")
                .addToBackStack(null)
                .commit();
    }
}
