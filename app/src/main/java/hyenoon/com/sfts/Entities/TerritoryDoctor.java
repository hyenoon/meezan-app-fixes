package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TerritoryDoctor {

    @SerializedName("customers")
    @Expose
    private List<Doctor> customers = new ArrayList<Doctor>();

    /**
     * @return The doctors
     */
    public List<Doctor> getDoctors() {
        return customers;
    }

    /**
     * @param doctors The doctors
     */
    public void setDoctors(List<Doctor> doctors) {
        this.customers = doctors;
    }

}
