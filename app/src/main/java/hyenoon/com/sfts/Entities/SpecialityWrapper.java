
package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SpecialityWrapper {

    @SerializedName("speciality")
    @Expose
    private List<Speciality> speciality = new ArrayList<Speciality>();

    /**
     * 
     * @return
     *     The speciality
     */
    public List<Speciality> getSpeciality() {
        return speciality;
    }

    /**
     * 
     * @param speciality
     *     The speciality
     */
    public void setSpeciality(List<Speciality> speciality) {
        this.speciality = speciality;
    }

}
