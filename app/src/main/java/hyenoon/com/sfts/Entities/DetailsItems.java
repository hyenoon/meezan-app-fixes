package hyenoon.com.sfts.Entities;

/**
 * Created by us on 3/6/2018.
 */

public class DetailsItems {
    String kew;
    String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



    public DetailsItems(String key, String string) {
        this.kew=key;
        this.value=string;
    }

    public String getKew() {
        return kew;
    }

    public void setKew(String kew) {
        this.kew = kew;
    }
}
