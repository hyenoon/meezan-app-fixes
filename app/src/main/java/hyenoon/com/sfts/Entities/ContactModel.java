package hyenoon.com.sfts.Entities;

/**
 * Created by IMac on 22/02/2018.
 */

public class ContactModel {

    public String contactName;

    public String contactNumber;

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
}
