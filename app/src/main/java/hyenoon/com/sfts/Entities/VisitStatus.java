package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by us on 3/2/2018.
 */

public class VisitStatus {

    @SerializedName("status_id")
    @Expose
    private String statusId;
    @SerializedName("status_name")
    @Expose
    private String statusName;
    @SerializedName("status_description")
    @Expose
    private Object statusDescription;

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Object getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(Object statusDescription) {
        this.statusDescription = statusDescription;
    }

}
