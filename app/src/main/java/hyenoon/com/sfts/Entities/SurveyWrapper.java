package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SurveyWrapper {
	@SerializedName("Survey")
	@Expose
	private Survey _Survey;

	/**
	* 
	* @return
	* The Survey
	*/
	public Survey getSurvey() {
	return _Survey;
	}

	/**
	* 
	* @param Survey
	* The Survey
	*/
	public void setSurvey(Survey Survey) {
	this._Survey = Survey;
	}
}
