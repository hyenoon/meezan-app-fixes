package hyenoon.com.sfts.Entities;

/**
 * Created by us on 3/2/2018.
 */

public class CheckInModule {

    String userID;
    String customerPhoneNumber;
    String lat;
    String lng;
    String total_amount;
    int customer_status;
    String visit_type;
    String followup_date;
    String visited_time;
    String[] product_id;
    String customerName;
    String companyName;
    String emailAddress;
    String address;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String[] getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String[] product_id) {
        this.product_id = product_id;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public int getCustomer_status() {
        return customer_status;
    }

    public void setCustomer_status(int customer_status) {
        this.customer_status = customer_status;
    }

    public String getVisit_type() {
        return visit_type;
    }

    public void setVisit_type(String visit_type) {
        this.visit_type = visit_type;
    }

    public String getFollowup_date() {
        return followup_date;
    }

    public void setFollowup_date(String followup_date) {
        this.followup_date = followup_date;
    }

    public String getVisited_time() {
        return visited_time;
    }

    public void setVisited_time(String visited_time) {
        this.visited_time = visited_time;
    }

    public CheckInModule(String userID, String customerPhoneNumber, String lat, String lng, String total_amount, int customer_status, String visit_type,
                         String followup_date, String visited_time, String[] product_id, String customerName, String companyName, String emailAddress, String address) {
        this.userID = userID;
        this.customerPhoneNumber = customerPhoneNumber;
        this.lat = lat;
        this.lng = lng;
        this.total_amount = total_amount;
        this.customer_status = customer_status;
        this.visit_type = visit_type;
        this.followup_date = followup_date;
        this.visited_time = visited_time;
        this.product_id = product_id;
        this.customerName = customerName;
        this.companyName = companyName;
        this.emailAddress = emailAddress;
        this.address = address;
    }

    @Override
    public String toString() {
        return userID + customerPhoneNumber + lat + lng + product_id + total_amount + customer_status + visit_type + followup_date + visited_time;
    }
}
