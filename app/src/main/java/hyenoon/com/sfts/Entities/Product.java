package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Product {

	@SerializedName("product_id")
	@Expose
	private String id;
	@SerializedName("product_name")
	@Expose
	private String name;
	@SerializedName("company")
	@Expose
	private String company;
	@SerializedName("product_type")
	@Expose
	private String type;
	@SerializedName("competitive_product")
	@Expose
	private List<String> competitiveProduct = new ArrayList<String>();

	/**
	* 
	* @return
	* The id
	*/
	public String getId() {
	return id;
	}

	/**
	* 
	* @param id
	* The id
	*/
	public void setId(String id) {
	this.id = id;
	}

	/**
	* 
	* @return
	* The name
	*/
	public String getName() {
	return name;
	}

	/**
	* 
	* @param name
	* The name
	*/
	public void setName(String name) {
	this.name = name;
	}

	/**
	* 
	* @return
	* The company
	*/
	public String getCompany() {
	return company;
	}

	/**
	* 
	* @param company
	* The company
	*/
	public void setCompany(String company) {
	this.company = company;
	}

	/**
	* 
	* @return
	* The type
	*/
	public String getType() {
	return type;
	}

	/**
	* 
	* @param type
	* The type
	*/
	public void setType(String type) {
	this.type = type;
	}

	/**
	* 
	* @return
	* The competitiveProduct
	*/
	public List<String> getCompetitiveProduct() {
	return competitiveProduct;
	}

	/**
	* 
	* @param competitiveProduct
	* The competitive_product
	*/
	public void setCompetitiveProduct(List<String> competitiveProduct) {
	this.competitiveProduct = competitiveProduct;
	}

	@Override
	public String toString() {
		return name;
	}
}
