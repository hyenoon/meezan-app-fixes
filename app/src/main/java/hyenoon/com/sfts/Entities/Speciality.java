
package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Speciality {

    @SerializedName("speciality_id")
    @Expose
    private String specialityId;
    @SerializedName("speciality_name")
    @Expose
    private String specialityName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("comments")
    @Expose
    private Object comments;

    /**
     * 
     * @return
     *     The specialityId
     */
    public String getSpecialityId() {
        return specialityId;
    }

    /**
     * 
     * @param specialityId
     *     The speciality_id
     */
    public void setSpecialityId(String specialityId) {
        this.specialityId = specialityId;
    }

    /**
     * 
     * @return
     *     The specialityName
     */
    public String getSpecialityName() {
        return specialityName;
    }

    /**
     * 
     * @param specialityName
     *     The speciality_name
     */
    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The comments
     */
    public Object getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    public void setComments(Object comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return specialityName;
    }

}
