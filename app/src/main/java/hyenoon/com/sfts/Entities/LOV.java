package hyenoon.com.sfts.Entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by us on 3/2/2018.
 */

public class LOV {
    @SerializedName("products")
    @Expose
    private List<ProductLOV> products = null;
    @SerializedName("visit_status")
    @Expose
    private List<VisitStatus> visitStatus = null;

    public List<ProductLOV> getProducts() {
        return products;
    }

    public void setProducts(List<ProductLOV> products) {
        this.products = products;
    }

    public List<VisitStatus> getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(List<VisitStatus> visitStatus) {
        this.visitStatus = visitStatus;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
