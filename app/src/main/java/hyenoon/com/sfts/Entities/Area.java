
package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Area {

    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("area_code")
    @Expose
    private String areaCode;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * 
     * @return
     *     The areaId
     */
    public String getAreaId() {
        return areaId;
    }

    /**
     * 
     * @param areaId
     *     The area_id
     */
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    /**
     * 
     * @return
     *     The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * 
     * @param cityId
     *     The city_id
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * 
     * @return
     *     The areaName
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * 
     * @param areaName
     *     The area_name
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * 
     * @return
     *     The areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 
     * @param areaCode
     *     The area_code
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return areaName;
    }
}
