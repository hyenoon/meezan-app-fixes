package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Question {

//	@SerializedName("id")
//	@Expose
	private String id;
	
//	@SerializedName("question")
//	@Expose
	private String question;
	
//	@SerializedName("qtype")
//	@Expose
	private String type;
	
//	@SerializedName("isqty")
//	@Expose
	private String isqty;
	
	private String company="";
	
//	@SerializedName("question_choices")
//	@Expose
	private List<String> questionChoices = new ArrayList<String>();


	/**
	* 
	* @return
	* The id
	*/
	public String getQty() {
	return isqty;
	}

	/**
	* 
	* @param id
	* The id
	*/
	public void setQty(String isqty) {
	this.isqty = isqty;
	}
	
	/**
	* 
	* @return
	* The id
	*/
	public String getId() {
	return id;
	}

	/**
	* 
	* @param id
	* The id
	*/
	public void setId(String id) {
	this.id = id;
	}

	/**
	* 
	* @return
	* The question
	*/
	public String getQuestion() {
	return question;
	}

	/**
	* 
	* @param question
	* The question
	*/
	public void setQuestion(String question) {
	this.question = question;
	}

	/**
	* 
	* @return
	* The type
	*/
	public String getType() {
	return type;
	}

	/**
	* 
	* @param type
	* The type
	*/
	public void setType(String type) {
	this.type = type;
	}

	/**
	* 
	* @return
	* The questionChoices
	*/
	public List<String> getQuestionChoices() {
	return questionChoices;
	}

	/**
	* 
	* @param questionChoices
	* The question_choices
	*/
	public void setQuestionChoices(List<String> questionChoices) {
	this.questionChoices = questionChoices;
	}

}
