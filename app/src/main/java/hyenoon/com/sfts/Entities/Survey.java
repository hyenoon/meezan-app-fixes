package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Survey {
	@SerializedName("questions")
	@Expose
	private List<Question> questions = new ArrayList<Question>();
	@SerializedName("products")
	@Expose
	private List<Product> products = new ArrayList<Product>();

	/**
	 * 
	 * @return The questions
	 */
	public List<Question> getQuestions() {
		return questions;
	}

	/**
	 * 
	 * @param questions
	 *            The questions
	 */
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	/**
	 * 
	 * @return The products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * 
	 * @param products
	 *            The products
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
