package hyenoon.com.sfts.Entities;

/**
 * Created by us on 3/2/2018.
 */

public class ProductModel {

    String productID;
    int productIndex;

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public int getProductIndex() {
        return productIndex;
    }

    public void setProductIndex(int productIndex) {
        this.productIndex = productIndex;
    }

}
