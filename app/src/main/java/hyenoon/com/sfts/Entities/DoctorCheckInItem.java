package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by DEV.Akber on 7/19/2016.
 */
public class DoctorCheckInItem {

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("doctorid")
    @Expose
    private String doctorId;

    @SerializedName("lat")
    @Expose
    private String latitude;

    @SerializedName("lng")
    @Expose
    private String longitude;

    @SerializedName("visited_time")
    @Expose
    private long currentTime;

    @SerializedName("products")
    @Expose
    private String products;

    @SerializedName("questions")
    @Expose
    private String questions;

    private Doctor doctor;

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The doctor_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The doctorId
     */
    public String getDoctorId() {
        return doctorId;
    }

    /**
     *
     * @param doctorId
     * The doctor_id
     */
    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    /**
     *
     * @return
     * The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return
     * The currentTime
     */
    public long getCurrentTime() {
        return currentTime;
    }

    /**
     * @param currentTime
     * The currentTime
     */
    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    /**
     *
     * @return
     * The products
     */
    public String getProducts() {
        return products;
    }

    /**
     *
     * @param products
     * The products
     */
    public void setProducts(String products) {
        this.products = products;
    }

    /**
     *
     * @return
     * The questions
     */
    public String getQuestions() {
        return questions;
    }

    /**
     *
     * @param questions
     * The questions
     */
    public void setQuestions(String questions) {
        this.questions = questions;
    }

    /**
     * @return
     * The doctor
     */
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     * @param doctor
     * The Doctor
     */
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
}

