package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserWrapper {
	@SerializedName("users")
	@Expose
	private Users users;

	/**
	* 
	* @return
	* The users
	*/
	public Users getUsers() {
	return users;
	}

	/**
	* 
	* @param users
	* The users
	*/
	public void setUsers(Users users) {
	this.users = users;
	}

}
