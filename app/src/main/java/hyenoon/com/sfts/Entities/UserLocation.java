package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dev.akber on 7/22/2016.
 */
public class UserLocation {

    @SerializedName("userId")
    private String userId;

    @SerializedName("lat")
    private String lat;

    @SerializedName("long")
    private String lng;

    @SerializedName("location_time")
    private long locationTime;

    @SerializedName("versionNo")
    private String versionNo;


    @SerializedName("batteryLevel")
    private String batteryLevel;

    public String getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(String batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    /**
     *
     * @return
     * The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The lat
     */
    public String getLat() {
        return lat;
    }

    /**
     *
     * @param lat
     * The lat
     */
    public void setLat(String lat) {
        this.lat = lat;
    }

    /**
     *
     * @return
     * The lng
     */
    public String getLng() {
        return lng;
    }

    /**
     *
     * @param lng
     * The lng
     */
    public void setLng(String lng) {
        this.lng = lng;
    }

    /**
     *
     * @return
     * The locationTime
     */
    public long getLocationTime () {
        return locationTime;
    }

    /**
     *
     * @param locationTime
     * The locationTime
     */
    public void setLocationTime(long locationTime) {
        this.locationTime = locationTime;
    }

    /**
     *
     * @return
     * The versionNo
     */
    public String getVersionNo() {
        return versionNo;
    }

    /**
     *
     * @param versionNo
     * The versionNo
     */
    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }
}
