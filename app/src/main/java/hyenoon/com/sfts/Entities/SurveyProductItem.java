package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dev.akber on 7/18/2016.
 */
public class SurveyProductItem {

    @SerializedName("selectedProduct")
    @Expose
    private String selectedProduct;

    @SerializedName("selectedCompetitiveProduct")
    @Expose
    private String selectedcompetitiveProduct;

    /**
     *
     * @return
     * The selectedcompetitiveProduct
     */
    public String getSelectedcompetitiveProduct() {
        return selectedcompetitiveProduct;
    }

    /**
     *
     * @param selectedcompetitiveProduct
     * The selectedcompetitiveProduct
     */
    public void setSelectedcompetitiveProduct(String selectedcompetitiveProduct) {
        this.selectedcompetitiveProduct = selectedcompetitiveProduct;
    }

    /**
     *
     * @return
     * The selectedProduct
     */
    public String getSelectedProduct() {
        return selectedProduct;
    }

    /**
     *
     * @param selectedProduct
     * The selectedProduct
     */
    public void setSelectedProduct(String selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

}
