package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by us on 3/2/2018.
 */

public class BodyLOV {
    @SerializedName("lov")
    @Expose
    private LOV lov;

    public LOV getLov() {
        return lov;
    }

    public void setLov(LOV lov) {
        this.lov = lov;
    }
}
