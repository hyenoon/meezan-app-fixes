package hyenoon.com.sfts.Entities;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Doctor {

	private String doctorDetails;
	@SerializedName("customer_id")
	@Expose
	private String customerId;
	@SerializedName("customer_name")
	@Expose
	private String customerName;
	@SerializedName("company_name")
	@Expose
	private String companyName;
	@SerializedName("email_address")
	@Expose
	private String emailAddress;
	@SerializedName("customer_type")
	@Expose
	private String customerType;
	@SerializedName("address")
	@Expose
	private String address;
	@SerializedName("contact_number")
	@Expose
	private String contactNumber;
	@SerializedName("image_url")
	@Expose
	private Object imageUrl;
	@SerializedName("category_id")
	@Expose
	private Object categoryId;
	@SerializedName("latitude")
	@Expose
	private String latitude;
	@SerializedName("longitude")
	@Expose
	private String longitude;
	@SerializedName("area_id")
	@Expose
	private Object areaId;
	@SerializedName("region_id")
	@Expose
	private Object regionId;
	@SerializedName("city_id")
	@Expose
	private Object cityId;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("details")
	@Expose
	private List<String> details;
//	@SerializedName("details")
//	@Expose
//	private ExtraCustomerDetails details;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Object getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(Object imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Object getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Object categoryId) {
		this.categoryId = categoryId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Object getAreaId() {
		return areaId;
	}

	public void setAreaId(Object areaId) {
		this.areaId = areaId;
	}

	public Object getRegionId() {
		return regionId;
	}

	public void setRegionId(Object regionId) {
		this.regionId = regionId;
	}

	public Object getCityId() {
		return cityId;
	}

	public void setCityId(Object cityId) {
		this.cityId = cityId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}

	//	public ExtraCustomerDetails getDetails() {
//		return details;
//	}
//
//	public void setDetails(ExtraCustomerDetails details) {
//		this.details = details;
//	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
}
