package hyenoon.com.sfts.Entities;

import android.location.Location;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Users {
	
	@SerializedName("user_id")
	@Expose
	private String userId;
	@SerializedName("login_id")
	@Expose
	private String loginId;
	@SerializedName("password")
	@Expose
	private String password;
	@SerializedName("first_name")
	@Expose
	private String firstName;
	@SerializedName("last_name")
	@Expose
	private String lastName;
	@SerializedName("email_address")
	@Expose
	private String emailAddress;
	@SerializedName("phone_number")
	@Expose
	private String phoneNumber;
	@SerializedName("dob")
	@Expose
	private String dob;
	@SerializedName("gender")
	@Expose
	private String gender;
	@SerializedName("user_type")
	@Expose
	private String userType;
	@SerializedName("country")
	@Expose
	private String country;
	@SerializedName("city")
	@Expose
	private Object city;
	@SerializedName("province")
	@Expose
	private Object province;
	@SerializedName("area")
	@Expose
	private Object area;
	@SerializedName("creation_date")
	@Expose
	private String creationDate;
	@SerializedName("status")
	@Expose
	private String status;
	@SerializedName("comments")
	@Expose
	private Object comments;
	@SerializedName("shift_status")
	@Expose
	private Object shiftStatus;

	private Location mCurrentLocation;

	/**
	* 
	* @return
	* The userId
	*/
	public String getUserId() {
	return userId;
	}

	/**
	* 
	* @param userId
	* The user_id
	*/
	public void setUserId(String userId) {
	this.userId = userId;
	}

	/**
	* 
	* @return
	* The loginId
	*/
	public String getLoginId() {
	return loginId;
	}

	/**
	* 
	* @param loginId
	* The login_id
	*/
	public void setLoginId(String loginId) {
	this.loginId = loginId;
	}

	/**
	* 
	* @return
	* The password
	*/
	public String getPassword() {
	return password;
	}

	/**
	* 
	* @param password
	* The password
	*/
	public void setPassword(String password) {
	this.password = password;
	}

	/**
	* 
	* @return
	* The firstName
	*/
	public String getFirstName() {
	return firstName;
	}

	/**
	* 
	* @param firstName
	* The first_name
	*/
	public void setFirstName(String firstName) {
	this.firstName = firstName;
	}

	/**
	* 
	* @return
	* The lastName
	*/
	public String getLastName() {
	return lastName;
	}

	/**
	* 
	* @param lastName
	* The last_name
	*/
	public void setLastName(String lastName) {
	this.lastName = lastName;
	}

	/**
	* 
	* @return
	* The emailAddress
	*/
	public String getEmailAddress() {
	return emailAddress;
	}

	/**
	* 
	* @param emailAddress
	* The email_address
	*/
	public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
	}

	/**
	* 
	* @return
	* The phoneNumber
	*/
	public String getPhoneNumber() {
	return phoneNumber;
	}

	/**
	* 
	* @param phoneNumber
	* The phone_number
	*/
	public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
	}

	/**
	* 
	* @return
	* The dob
	*/
	public String getDob() {
	return dob;
	}

	/**
	* 
	* @param dob
	* The dob
	*/
	public void setDob(String dob) {
	this.dob = dob;
	}

	/**
	* 
	* @return
	* The gender
	*/
	public String getGender() {
	return gender;
	}

	/**
	* 
	* @param gender
	* The gender
	*/
	public void setGender(String gender) {
	this.gender = gender;
	}

	/**
	* 
	* @return
	* The userType
	*/
	public String getUserType() {
	return userType;
	}

	/**
	* 
	* @param userType
	* The user_type
	*/
	public void setUserType(String userType) {
	this.userType = userType;
	}

	/**
	* 
	* @return
	* The country
	*/
	public String getCountry() {
	return country;
	}

	/**
	* 
	* @param country
	* The country
	*/
	public void setCountry(String country) {
	this.country = country;
	}

	/**
	* 
	* @return
	* The city
	*/
	public Object getCity() {
	return city;
	}

	/**
	* 
	* @param city
	* The city
	*/
	public void setCity(Object city) {
	this.city = city;
	}

	/**
	* 
	* @return
	* The province
	*/
	public Object getProvince() {
	return province;
	}

	/**
	* 
	* @param province
	* The province
	*/
	public void setProvince(Object province) {
	this.province = province;
	}

	/**
	* 
	* @return
	* The area
	*/
	public Object getArea() {
	return area;
	}

	/**
	* 
	* @param area
	* The area
	*/
	public void setArea(Object area) {
	this.area = area;
	}

	/**
	* 
	* @return
	* The creationDate
	*/
	public String getCreationDate() {
	return creationDate;
	}

	/**
	* 
	* @param creationDate
	* The creation_date
	*/
	public void setCreationDate(String creationDate) {
	this.creationDate = creationDate;
	}

	/**
	* 
	* @return
	* The status
	*/
	public String getStatus() {
	return status;
	}

	/**
	* 
	* @param status
	* The status
	*/
	public void setStatus(String status) {
	this.status = status;
	}

	/**
	* 
	* @return
	* The comments
	*/
	public Object getComments() {
	return comments;
	}

	/**
	* 
	* @param comments
	* The comments
	*/
	public void setComments(Object comments) {
	this.comments = comments;
	}

	/**
	 *
	 * @return
	 * The shiftStatus
	 */
	public Object getShiftStatus() {
		return shiftStatus;
	}

	/**
	 *
	 * @param shiftStatus
	 * The ShiftStatus
	 */
	public void setShiftStatus(Object shiftStatus) {
		this.shiftStatus = shiftStatus;
	}

	/**
	 * @return
	 */
	public Location getmCurrentLocation() {
		//Log.e("Location ",String.valueOf(mCurrentLocation.getLatitude()));
		return mCurrentLocation;
	}

	/**
	 * @param mCurrentLocation
	 */
	public void setmCurrentLocation(Location mCurrentLocation) {
		this.mCurrentLocation = mCurrentLocation;
	}
}
