package hyenoon.com.sfts.Entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by us on 3/1/2018.
 */

public class TeamCustomersBody {

    @SerializedName("customers")
    @Expose
    private List<TeamCustomers> customers = null;

    public List<TeamCustomers> getCustomers() {
        return customers;
    }

    public void setCustomers(List<TeamCustomers> customers) {
        this.customers = customers;
    }

}
