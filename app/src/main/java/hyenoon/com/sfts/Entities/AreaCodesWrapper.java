
package hyenoon.com.sfts.Entities;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AreaCodesWrapper {

    @SerializedName("area")
    @Expose
    private List<Area> area = new ArrayList<Area>();

    /**
     * 
     * @return
     *     The area
     */
    public List<Area> getArea() {
        return area;
    }

    /**
     * 
     * @param area
     *     The area
     */
    public void setArea(List<Area> area) {
        this.area = area;
    }

}
