package hyenoon.com.sfts;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.Sheet;
import hyenoon.com.sfts.Entities.SheetBody;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportPerfomanceActivity extends BaseActivity {

    RecyclerView rv_RecyclerView;
    TextView tv_Warnning;
    ReportPerfomanceAdapter mAdapter;
    Context context;
    private String TAG = getClass().getName();
    List<Sheet> sheetList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_perfomance);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Performance Report");
        }
        context = this;
        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        callAPI();
    }

    private void callAPI() {

        Log.e(TAG, "UserId " + prefHelper.getUser().getUserId());
        pDialog = Utils.showSweetLoader(ReportPerfomanceActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Fetching Customers");
        Call<WebResponse<SheetBody>> call = WebServiceFactory.getInstance().getPerfomanceSheet(Constants.GET_PERFORMANCE_SHEET, prefHelper.getUser().getUserId());
        call.enqueue(new Callback<WebResponse<SheetBody>>() {
            @Override
            public void onResponse(Call<WebResponse<SheetBody>> call, Response<WebResponse<SheetBody>> response) {
                Utils.hideSweetLoader(pDialog);

                if (response.body() == null)
                {

                    Toast.makeText(ReportPerfomanceActivity.this, "Something went wrong,check internet connectivity", Toast.LENGTH_SHORT).show();
                    Log.e(TAG,"null");
                }else if(response.body().isSuccess() && response.body().getBody().getSheets().size()>0) {
                    Log.e(TAG,"Set Adapter");
                    sheetList = response.body().getBody().getSheets();
                    mAdapter.notifyDataSetChanged();
                }
                else{
                    findViewById(R.id.tv_warnning).setVisibility(View.VISIBLE);
                    Log.e(TAG, response.body().getMessage());
                    Log.e(TAG,"Success "+response.body().isSuccess());
                   // Toast.makeText(ReportPerfomanceActivity.this, "Data not Found", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Error? " + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SheetBody>> call, Throwable t) {
                Utils.hideSweetLoader(pDialog);

                Log.e(TAG, "onFailure " + t.toString());
                Toast.makeText(ReportPerfomanceActivity.this, "Something went wrong,check internet connectivity", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void init() {

        tv_Warnning = (TextView) findViewById(R.id.tv_warnning);
        rv_RecyclerView = (RecyclerView) findViewById(R.id.rv_reportperfomance_recycler_view);
        mAdapter = new ReportPerfomanceAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rv_RecyclerView.setLayoutManager(mLayoutManager);
        rv_RecyclerView.setItemAnimator(new DefaultItemAnimator());
        rv_RecyclerView.setAdapter(mAdapter);
        if (Utils.isNetworkConnected(context)) {
            rv_RecyclerView.setVisibility(View.VISIBLE);
            tv_Warnning.setVisibility(View.GONE);

        } else {
            tv_Warnning.setVisibility(View.VISIBLE);
            rv_RecyclerView.setVisibility(View.GONE);
        }
    }

    private class ReportPerfomanceAdapter extends RecyclerView.Adapter<ReportPerfomanceAdapter.VH> {

        Context context;

        public ReportPerfomanceAdapter(Context context) {
            this.context = context;
        }

        @Override
        public ReportPerfomanceAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_reportperfomance, parent, false);
            return new VH(v);

        }

        @Override
        public void onBindViewHolder(ReportPerfomanceAdapter.VH holder, final int position) {
            holder.tv_Name.setText("File : " + sheetList.get(position).getFileName());
            holder.tv_Number.setText("Date : " + sheetList.get(position).getDate());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String fileName = URLUtil.guessFileName(sheetList.get(position).getFilePath(), null, null);
                    File file = new File(Environment.getExternalStorageDirectory() + "/Meezan Bank File/", fileName);
                    Log.e(TAG, "File name " + fileName);
                    Log.e(TAG, "File Path " + file.getAbsolutePath());
                    startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(sheetList.get(position).getFilePath())));

//                   if (file.exists()) {
//                        Log.e(TAG, "File Path " + file.getAbsolutePath());
//
//                        Uri uri = Uri.fromFile(file);
//                        Intent intent = new Intent(Intent.ACTION_VIEW);
//                        intent.setDataAndType(uri, "**/*//*");
//                        intent.setDataAndType(uri, "application/vnd.ms-excel");
//                        startActivity(intent);
//                    }
//                    else {
//                        Log.e(TAG, "File not exist ");
//                        DownloadManager downloadmanager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
//                        Uri uri = Uri.parse(sheetList.get(position).getFilePath());
//                        DownloadManager.Request request = new DownloadManager.Request(uri);
//                        request.setTitle(fileName);
//                        request.setDescription("Downloading");
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                        request.setDestinationUri(Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/Meezan Bank File" + "/" + fileName));
//                        downloadmanager.enqueue(request);
//                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return sheetList.size();
        }

        public class VH extends RecyclerView.ViewHolder {
            TextView tv_Name, tv_Number;

            public VH(View itemView) {
                super(itemView);
                tv_Name = (TextView) itemView.findViewById(R.id.tv_name);
                tv_Number = (TextView) itemView.findViewById(R.id.tv_number);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
