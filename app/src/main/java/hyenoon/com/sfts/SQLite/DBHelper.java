package hyenoon.com.sfts.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by us on 2/21/2018.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MeezanBank.db";
    private static final String TABLE_LCOATION = "LOCATION";
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table if not exists LOCATION " +
                "(id integer primary key, lat double,long double)"
      );
       // onCreate(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_LCOATION);
    }

    public boolean insertLcoation(double Lat, double Long) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("lat", Lat);
        contentValues.put("long", Long);
        db.insert(TABLE_LCOATION, null, contentValues);
        db.close();
        return true;
    }

    public ArrayList<String> getLocations() {

        ArrayList<String> list = new ArrayList<String>();

        // Select All Query
        String selectQuery = "select * from "+TABLE_LCOATION;

        SQLiteDatabase db = this.getReadableDatabase();
        try {

            Cursor cursor = db.rawQuery(selectQuery, null);
            try {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {

                        for(int i = 0 ; i < cursor.getColumnCount(); i++){
                            list.add(cursor.getString(i));
                        }

                    } while (cursor.moveToNext());
                }

            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } finally {
            try { db.close(); } catch (Exception ignore) {}
        }

        return list;
    }

    public boolean clearAll(){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_LCOATION,null,null);
        db.close();
        return true;
    }
}
