package hyenoon.com.sfts;

import android.app.Application;

import com.evernote.android.job.JobManager;

import hyenoon.com.sfts.job_schedule.SyncCallsJobCreator;

/**
 * Created by us on 3/9/2018.
 */

public class App extends Application {
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
////        fix();
//    }

    @Override
    public void onCreate() {
        super.onCreate();
        JobManager.create(this).addJobCreator(new SyncCallsJobCreator());
    }

//    public static void fix() {
//        try {
//            Class clazz = Class.forName("java.lang.Daemons$FinalizerWatchdogDaemon");
//
//            Method method = clazz.getSuperclass().getDeclaredMethod("stop");
//            method.setAccessible(true);
//
//            Field field = clazz.getDeclaredField("INSTANCE");
//            field.setAccessible(true);
//
//            method.invoke(field.get(null));
//
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
//    }
}
