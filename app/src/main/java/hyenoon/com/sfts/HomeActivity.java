package hyenoon.com.sfts;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.Area;
import hyenoon.com.sfts.Entities.AreaCodesWrapper;
import hyenoon.com.sfts.Entities.BodyLOV;
import hyenoon.com.sfts.Entities.Speciality;
import hyenoon.com.sfts.Entities.SpecialityWrapper;
import hyenoon.com.sfts.Entities.Survey;
import hyenoon.com.sfts.Entities.SurveyWrapper;
import hyenoon.com.sfts.Entities.TeamCustomersBody;
import hyenoon.com.sfts.Entities.TerritoryDoctor;
import hyenoon.com.sfts.HelperClasses.LoginPreference;
import hyenoon.com.sfts.Location.SimpleLocation;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Services.FusedLocationProviderService;
import hyenoon.com.sfts.Services.SyncService;
import hyenoon.com.sfts.Utils.Utils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.view.View.GONE;

@RuntimePermissions
public class HomeActivity extends BaseActivity {

    private static String TAG = "HomeActivity";
    private UiUpdateRecevier uiUpdateRecevier;
    private GPSReceiver mGpsReceiver;
    private NetworkReceiver mNetworkReceiver;
    private Button btnStartShift;
    private Button btnEndShift;
    private AlertDialog alertDialog;
    Snackbar mSnackBar;
    LinearLayout llDebugContainer;
    //    private TextView tvMissedSyncCalls, tvLocationZero;
    private TextView tvLatitude, tvLongitude, tvTrackingCalls, tvCheckinCalls, tvInternetStatus, tvGPSStatus;

    private SimpleLocation mLocation;

    boolean toggleStatus = true;

    boolean isPermission =false;
    String userType;

    Context context;
    ImageView iv_ColdCall, iv_CheckInByPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;
        init();
        userType = prefHelper.getUser().getUserType();

        Log.e(TAG, "User Type " + userType);
        if (userType.equals(Constants.BDO)) {
            Log.e(TAG, prefHelper.getUser().getUserId());
            Log.e(TAG, "User Type is BDO");
            setVisibility(iv_ColdCall, View.VISIBLE);
        } else {
            Log.e(TAG, prefHelper.getUser().getUserId());
            Log.e(TAG, "User Type is Other");
            setVisibility(iv_CheckInByPhone, View.VISIBLE);
        }

        if (getSupportActionBar() != null
                && prefHelper.getUser() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle(prefHelper.getUser().getFirstName());


        }


        mLocation = SimpleLocation.getInstance(this);
        btnStartShift = (Button) findViewById(R.id.buttonStartShift);
        btnEndShift = (Button) findViewById(R.id.buttonEndShift);
        tvLatitude = (TextView) findViewById(R.id.tvLatitude);
        tvLongitude = (TextView) findViewById(R.id.tvLongitude);
        tvTrackingCalls = (TextView) findViewById(R.id.tvTrackingCalls);
        tvCheckinCalls = (TextView) findViewById(R.id.tvCheckinCalls);
        tvInternetStatus = (TextView) findViewById(R.id.tvInternetStatus);
        tvGPSStatus = (TextView) findViewById(R.id.tvGPSStatus);


        //tvMissedSyncCalls = (TextView) findViewById(tvMissedSyncCalls);
        //tvLocationZero = (TextView) findViewById(tvLocationZero);
        uiUpdateRecevier = new UiUpdateRecevier();
        mGpsReceiver = new GPSReceiver();
        mNetworkReceiver = new NetworkReceiver();


        tvGPSStatus.setText((mLocation.hasLocationEnabled()) ? "GPS ON" : "GPS OFF");
        fetchSurveyDetails();
        fetchSpecialities();
        fetchAreaCodes(prefHelper.getUser().getUserId());
        //   SyncService.setToast(getApplicationContext());

        startServiceClass(SyncService.class);
    }

    @NeedsPermission({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void setupContactAccess() {
        isPermission =true;
        if (mSnackBar!=null)
            mSnackBar.dismiss();


    }


    @OnShowRationale({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void showStorageAccessRationale(final PermissionRequest request) {
        openSettinPage();
        request.proceed();
    }

    @OnPermissionDenied({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void storageAccessDenied() {
        isPermission =false;
//        Utils.showShortToastInCenter(this, "Access Denied, Please grant access to complete process");
        mSnackBar = Snackbar.make(llDebugContainer, "All Permissions are required,please Proceed", Snackbar.LENGTH_INDEFINITE).setAction("Proceed", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivityPermissionsDispatcher.setupContactAccessWithPermissionCheck(HomeActivity.this);
            }
        });

        mSnackBar.show();
    }

    @OnNeverAskAgain({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void openSetting() {
        isPermission =false;
        mSnackBar = Snackbar.make(llDebugContainer, "All Permissions are required.", Snackbar.LENGTH_INDEFINITE).setAction("Setting", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSettinPage();
            }
        });

        mSnackBar.show();
     //
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        HomeActivityPermissionsDispatcher.onRequestPermissionsResult(HomeActivity.this, requestCode, grantResults);
    }

    private void openSettinPage() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }


    private void init() {
        llDebugContainer = (LinearLayout) findViewById(R.id.llayout);
        iv_CheckInByPhone = (ImageView) findViewById(R.id.imgCheckInByPhones);
        iv_ColdCall = (ImageView) findViewById(R.id.imgColdCalls);
    }

    private void setVisibility(View view, int state) {
        view.setVisibility(state);
    }

    private void fetchData() {
//        pDialog = Utils.showSweetLoader(HomeActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Fetching");
        Log.e(TAG, "Sync Call");
        fetchDoctorsFromUserTerritory(prefHelper.getUser().getUserId());
        fetchLOV();
        if (!userType.equals(Constants.BDO)) {
            Log.e(TAG, "Sync not Called");
            fetchMyTeamCustomers(prefHelper.getUser().getUserId());
            //fetchLOV();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.homemenu, menu);
        Switch mSwtivh = (Switch) menu.findItem(R.id.action_tooglebtn).getActionView();
        mSwtivh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    findViewById(R.id.llayout).setVisibility(View.VISIBLE);
                    Log.e(TAG, "action_tooglebtn true");


                } else {
                    Log.e(TAG, "action_tooglebtn false");
                    findViewById(R.id.llayout).setVisibility(View.GONE);
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refreshBtn:
                fetchData();
                return true;
            case R.id.action_logoutbtn:
                final SweetAlertDialog dialog = new SweetAlertDialog(context);
                dialog.setConfirmText("Yes")
                        .setCancelText("No")
                        .setTitleText("Do you want to logout?")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                LoginPreference.setLoginStatus(context, false);
                                prefHelper.clearPref(context);
                                finish();
                                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);

                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                dialog.dismiss();
                            }
                        });
                dialog.show();
                return true;
            case R.id.action_tooglebtn:

                Log.e(TAG, "action_tooglebtn clicked");

                if (toggleStatus) {
                    Log.e(TAG, "action_tooglebtn true");

                    findViewById(R.id.llayout).setVisibility(View.GONE);
                    toggleStatus = false;

                } else {
                    Log.e(TAG, "action_tooglebtn false");

                    findViewById(R.id.llayout).setVisibility(View.VISIBLE);
                    toggleStatus = true;

                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        HomeActivityPermissionsDispatcher.setupContactAccessWithPermissionCheck(HomeActivity.this);

        LocalBroadcastManager.getInstance(this).registerReceiver(uiUpdateRecevier, new IntentFilter(Constants.kUIACTION));
        LocalBroadcastManager.getInstance(this).registerReceiver(mGpsReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }


    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(uiUpdateRecevier);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNetworkReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGpsReceiver);
        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();
        userID = prefHelper.getUser().getUserId();
        Log.e(TAG, "userID " + userID);

        checkAppVersion(Utils.getAppVersion(this));

        if (validateShift()) {

            btnStartShift.setVisibility(GONE);
            btnEndShift.setVisibility(View.VISIBLE);

            if (checkServiceRunning(FusedLocationProviderService.class))
                stopServiceClass(FusedLocationProviderService.class); // it will restart service in 10secs
            else
                startServiceClass(FusedLocationProviderService.class); // start service if not running.
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public void onClickReportperfomance(View view) {
        if (!isPermission)
            return;
            if (validateShift())

                startActivity(new Intent(this, ReportPerfomanceActivity.class));
            else
                Utils.showErrorAlertDialog(this, "Please start your shift");

    }

    public void onClickCheckInByPhone(View view) {
        if (!isPermission)
            return;
        if (validateShift()) {
            userID = prefHelper.getUser().getUserId();
            Log.e(TAG, "userID " + userID);
            startActivity(new Intent(this, CallCheckInActivity.class));
        } else
            Utils.showErrorAlertDialog(this, "Please start your shift");
    }

    public void onClickNearByCustomer(View view) {
        if (!isPermission)
            return;
        if (validateShift()) {
            userID = prefHelper.getUser().getUserId();
            Log.e(TAG, "userID " + userID);
            startActivity(new Intent(this, NearbyCustomersActivity.class));
        } else
            Utils.showErrorAlertDialog(this, "Please start your shift");
    }

    public void onClickMyCustomer(View view) {
        if (!isPermission)
            return;
        if (prefHelper.getShiftStatus())

            startActivity(new Intent(HomeActivity.this, DoctorActivity.class));
        else
            Utils.showErrorAlertDialog(this, "Please start your shift");
    }

    public void onClickStartShift(View view) {
        if (!isPermission)
            return;
        prefHelper.putShiftStatus(true);

        long currentTime = Calendar.getInstance().getTimeInMillis();
        Log.e(TAG, "Time " + currentTime);

        storeStartTime(currentTime);

        // 8 March, Nazar
        startServiceClass(FusedLocationProviderService.class);


        btnStartShift.setVisibility(GONE);
        btnEndShift.setVisibility(View.VISIBLE);
    }

    public void onClickEndShift(View view) {
        if (!isPermission)
            return;
        prefHelper.putShiftStatus(false);

        long currentTime = Calendar.getInstance().getTimeInMillis();

        Log.e(TAG, "Time " + currentTime);

        storeEndTime(currentTime);

        // 8 March, Nazar
        stopServiceClass(FusedLocationProviderService.class);

        btnStartShift.setVisibility(View.VISIBLE);

        btnEndShift.setVisibility(GONE);
    }

    public void onClickColdCall(View view) {
        if (!isPermission)
            return;
        if (validateShift())
            startActivity(new Intent(this, CallCheckInActivity.class));
        else
            Utils.showErrorAlertDialog(this, "Please start your shift");
    }

    private boolean validateShift() {

        if (prefHelper.getShiftStatus() == true)
            return true;
        else
            return false;

    }


   /* public void onClickDoctorList(View view) {

        if (prefHelper.getShiftStatus() == true)

            startActivity(new Intent(HomeActivity.this, DoctorActivity.class));
        else
            Utils.showErrorAlertDialog(this, "Please start your shift");
    }

    public void onClickDoctorCheckedInHistory(View view) {
        startActivity(new Intent(HomeActivity.this, DoctorCheckInHistory.class));
    }

    public void onClickLogout(View view) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Are you sure you want to logout");
        dialog.setTitle("Confirmation");

        dialog.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                prefHelper.putUser(null);
                prefHelper.putDoctorsList(null);
                prefHelper.putAreaCodeList(null);
                prefHelper.putShiftStatus(false);
                stopServiceClass(FusedLocationProviderService.class);
                stopServiceClass(SyncService.class);
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

     public void onClickMakeCall(View view) {
        startActivity(new Intent(this, MakeCallActivity.class));

    }*/


    private void fetchMyTeamCustomers(String useID) {
        Log.e(TAG, "fetchMyTeamCustomers called");
//        pDialog = Utils.showSweetLoader(HomeActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Fetching Customers");

        Call<WebResponse<TeamCustomersBody>> service = WebServiceFactory.getInstance().getTeamCustomers
                (Constants.kWebGetTeamCustomers, useID);

        service.enqueue(new Callback<WebResponse<TeamCustomersBody>>() {
            @Override
            public void onResponse(Call<WebResponse<TeamCustomersBody>> call, Response<WebResponse<TeamCustomersBody>> response) {
                Log.e(TAG, "fetchMyTeamCustomers onResponse");
                Utils.hideSweetLoader(pDialog);

                if (response != null) {
                    if (response.body() != null && response.body().getHeader().success.equals("1")) {
                        if (response.body().getBody() != null) {
                            Log.e(TAG, "TeamCustomers Fetched");
                            prefHelper.putTeamCustomerList(response.body().getBody().getCustomers());
                        }

                    } else
                        Log.e(TAG, "Found null in webresponse fetchMyTeamCustomers");
                }
            }

            @Override
            public void onFailure(Call<WebResponse<TeamCustomersBody>> call, Throwable t) {
                Utils.hideSweetLoader(pDialog);

                Log.e("HomeActivity", "Failed to load webservice fetchMyTeamCustomers" + t.getLocalizedMessage());
            }
        });


    }

    private void fetchDoctorsFromUserTerritory(final String userId) {

        Log.e(TAG, "fetchDoctorsFromUserTerritory called");
        pDialog = Utils.showSweetLoader(HomeActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Fetching");

        Call<WebResponse<TerritoryDoctor>> service = WebServiceFactory.getInstance().getDoctors
                (Constants.kWebGetDoctors, userId);

        service.enqueue(new Callback<WebResponse<TerritoryDoctor>>() {
            @Override
            public void onResponse(Call<WebResponse<TerritoryDoctor>> call, Response<WebResponse<TerritoryDoctor>> response) {
                Log.e(TAG, "fetchDoctorsFromUserTerritory onResponse ");
                Utils.hideSweetLoader(pDialog);

                if (response != null) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {
                        Log.e(TAG, "fetchDoctorsFromUserTerritory Success");

                        if (response.body().getBody() != null) {
//                            prefHelper.putDoctorsList(response.body().getBody().getDoctors());
                            prefHelper.putCustomerList(response.body().getBody().getDoctors());
                            Log.e(TAG, "Fetch Data Called");
                            //Log.e(TAG, "Size " + prefHelper.getDoctorsList().size());
                            Log.e(TAG, "customers list " + new Gson().toJson(prefHelper.getDoctorsList()));

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<TerritoryDoctor>> call, Throwable t) {
                Utils.hideSweetLoader(pDialog);
                Log.e("HomeActivity", "Failed to load webservice fetchDoctorsFromUserTerritory " + t.getLocalizedMessage());
            }
        });
    }

    public void fetchLOV() {


        Log.e(TAG, "fetchLOV Called");

        Call<WebResponse<BodyLOV>> call = WebServiceFactory.getInstance().getLOV(Constants.GET_LOV);
        call.enqueue(new Callback<WebResponse<BodyLOV>>() {
            @Override
            public void onResponse(Call<WebResponse<BodyLOV>> call, Response<WebResponse<BodyLOV>> response) {
//                Utils.hideSweetLoader(pDialog);
                if (response.body() == null)
                    Toast.makeText(HomeActivity.this, "something went wrong,please check your internet connectivity", Toast.LENGTH_SHORT).show();
                else if (response.body().isSuccess()) {
                    Log.e(TAG, "Size " + response.body().getBody().getLov().getProducts());
                    Log.e(TAG, "lov list json " + response.body().getBody().getLov().getProducts());
                    prefHelper.putLOV(response.body().getBody().getLov().getProducts());

                } else
                    Toast.makeText(HomeActivity.this, "something went wrong,please check your internet connectivity", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<WebResponse<BodyLOV>> call, Throwable t) {
//                Utils.hideSweetLoader(pDialog);

                Log.e(TAG, "onFailure " + t.getMessage());
                Toast.makeText(HomeActivity.this, "something went wrong,please check your internet connectivity", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void fetchSurveyDetails() {

        Call<WebResponse<SurveyWrapper>> service = WebServiceFactory.getInstance().getSurvey(Constants.kWebGetSurvey);
        service.enqueue(new Callback<WebResponse<SurveyWrapper>>() {
            @Override
            public void onResponse(Call<WebResponse<SurveyWrapper>> call, Response<WebResponse<SurveyWrapper>> response) {
                if (response != null) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {

                        if (response.body().getBody() != null) {
                            Survey survey = response.body().getBody().getSurvey();
                            prefHelper.putSurveyDetails(survey);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SurveyWrapper>> call, Throwable t) {
                Log.e("HomeActivity", "Failed to load survey details " + t.getLocalizedMessage());
            }
        });

    }

    private void fetchSpecialities() {

        Call<WebResponse<SpecialityWrapper>> service = WebServiceFactory.getInstance().getSpecialities(
                Constants.kWebGetSpecialities);
        service.enqueue(new Callback<WebResponse<SpecialityWrapper>>() {
            @Override
            public void onResponse(Call<WebResponse<SpecialityWrapper>> call, Response<WebResponse<SpecialityWrapper>> response) {
                if (response != null) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {

                        if (response.body().getBody() != null) {
                            List<Speciality> specialities = response.body().getBody().getSpeciality();
                            prefHelper.putSpecialityList(specialities);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SpecialityWrapper>> call, Throwable t) {
                Log.e(TAG, "Failed " + Constants.kWebGetSpecialities + ": " + t.getLocalizedMessage());
            }
        });

    }

    private void fetchAreaCodes(String userId) {

        Call<WebResponse<AreaCodesWrapper>> service = WebServiceFactory.getInstance().getAreaCodes(
                Constants.kWebGetAreaCodes, userId);
        service.enqueue(new Callback<WebResponse<AreaCodesWrapper>>() {
            @Override
            public void onResponse(Call<WebResponse<AreaCodesWrapper>> call, Response<WebResponse<AreaCodesWrapper>> response) {
                if (response != null) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {

                        if (response.body().getBody() != null) {
                            List<Area> areas = response.body().getBody().getArea();
                            prefHelper.putAreaCodeList(areas);
                        }

                    } else {
                        Log.e(TAG, "Failed to load webservice " + Constants.kWebGetAreaCodes);
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<AreaCodesWrapper>> call, Throwable t) {
                Log.e(TAG, "Failed " + Constants.kWebGetAreaCodes + ": " + t.getLocalizedMessage());
            }
        });
    }

    private void checkAppVersion(String version) {
        final Call<WebResponse<Object>> service = WebServiceFactory.getInstance().getVersionCheck(
                Constants.kWebCheckVersion, version);
        service.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response != null) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {

                        if (response.body().getBody() != null) {
                            Map<String, String> map = (Map<String, String>) response.body().getBody();
                            if (map.get("forceupgrade") != null
                                    && map.get("forceupgrade").equalsIgnoreCase("yes")) {

                                String url = map.get("url");
                                showUpdateDialog(url);
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                Log.e(TAG, "Failed " + Constants.kWebCheckVersion + ": " + t.getLocalizedMessage());
            }
        });
    }

    private void showUpdateDialog(final String url) {

        if (alertDialog == null || !alertDialog.isShowing()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("A New Update is Available");
            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "User quit app");
                    finish();
                }
            });

            builder.setCancelable(false);
            alertDialog = builder.show();
        }
    }

    private void actionCheckInternetStatus(Intent intent) {

        String status = intent.getStringExtra(Constants.kInternetStatus);
        tvInternetStatus.setText("Internet: " + status);
/*
        if (status != null && status.equals("Not Connected"))
            Utils.showLongToastInCenter(HomeActivity.this, R.string.internetStatus);
*/
    }

    private void actionLocation(Intent intent) {
        String lat = intent.getStringExtra(Constants.kLAT);
        String lng = intent.getStringExtra(Constants.kLONG);
        int locationZero = intent.getIntExtra(Constants.kLocationZero, -2);

        tvLatitude.setText("Lat: " + lat);
        tvLongitude.setText("Long: " + lng);
        Log.e(TAG, "loaction zero from broad " + locationZero);
        if (locationZero == 0)
            Utils.showLongToastInCenter(HomeActivity.this, R.string.locationZeroFound);
    }

    private void actionTrackingCalls(Intent intent) {

        int noOfTrackingCalls = intent.getIntExtra(Constants.kNoOfTrackingCalls, -1);
        tvTrackingCalls.setText("Tracking Calls: " + noOfTrackingCalls);


    }

    private void actionCheckinCalls(Intent intent) {

        int noOfCheckinCalls = intent.getIntExtra(Constants.kNoOfCheckinCalls, -1);
        tvCheckinCalls.setText("Checkin Calls: " + noOfCheckinCalls);

    }

    private void storeStartTime(final long currentTime) {

        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().startShiftTime(Constants.startShift,
                prefHelper.getUser().getUserId(), currentTime
        );
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body() != null && response.body().isSuccess()) {
                    Log.e(TAG, response.body().getMessage());
                } else if (!response.body().isSuccess()) {

                    Utils.showShortToastInCenter(getApplicationContext(), response.body().getHeader().message);
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {

                SharedPreferences preferences = getSharedPreferences(Constants.ShiftKey, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putLong("time", currentTime);
                editor.putString("shift", "Start Shift");
                editor.apply();
            }
        });

    }


    private void storeEndTime(final long currentTime) {

        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().endShiftTime(Constants.endShift,
                prefHelper.getUser().getUserId(), currentTime
        );
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body().isSuccess()) {
                    Log.e(TAG, response.body().getMessage());
                } else if (!response.body().isSuccess()) {

                    Utils.showShortToastInCenter(getApplicationContext(), response.body().getHeader().message);
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {

                SharedPreferences preferences = getSharedPreferences(Constants.ShiftKey, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putLong("time", currentTime);
                editor.putString("shift", "End Shift");
                editor.apply();
            }
        });

    }


    class UiUpdateRecevier extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "onReceive UiUpdateReceiver");
            String actionType = intent.getStringExtra(Constants.kACTION);

            switch (actionType) {
                case "location":
                    actionLocation(intent);
                    break;
                case "trackingCalls":
                    actionTrackingCalls(intent);
                    break;
                case "checkinCalls":
                    actionCheckinCalls(intent);
                    break;

                case "status":
                    actionCheckInternetStatus(intent);
                    break;


                default: {
                    Log.e(TAG, "Invalid action in recevier");
                }


            }// switch ends

        } // onReceive Ends
    } // class ends

    class GPSReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e(TAG, "onRecevie GPSRecevier");
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                if (!mLocation.hasLocationEnabled()) {
                    tvGPSStatus.setText("GPS is OFF");
                    tvLatitude.setText("Lat:");
                    tvLongitude.setText("Long:");
                } else {
                    tvGPSStatus.setText("GPS is ON");
                    tvLatitude.setText(mLocation.getLatitude() + "");
                    tvLongitude.setText(mLocation.getLongitude() + "");
                }

            }
        }
    }


    class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.e(TAG, "onRecevie NetWorkReceiver");
            if (Utils.isNetworkConnected(context)) {
                tvInternetStatus.setText("Internet: Connected");

            } else {
                tvInternetStatus.setText("Internet: Not Connected");
                //Utils.showLongToastInCenter(HomeActivity.this, R.string.internetStatus);
            }
        }
    }

    @Override
    public void onBackPressed() {
        exitDialog();
    }

    private void exitDialog(){
        final SweetAlertDialog dialog = new SweetAlertDialog(context);
        dialog.setConfirmText("Yes")
                .setCancelText("No")
                .setTitleText("Are you sure want to Exit?")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        dialog.dismiss();
                    }
                });
        dialog.show();
    }
}

