package hyenoon.com.sfts;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.Customer;
import hyenoon.com.sfts.Entities.CustomerBody;
import hyenoon.com.sfts.Entities.Users;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyCustomersActivity extends BaseActivity {

    RecyclerView rv_RecyclerView;
    TextView tv_Warnning;
    NearbyCustomersAdapter mAdapter;
    Context context;
    List<Customer> arrayList = new ArrayList<>();
    private String TAG = getClass().getName();
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby_customers);
        setContentView(R.layout.activity_nearby_customers);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("NearBy Customers");
        }
        context = this;

        callAPI(Constants.GET_NEARBY_CUSTOMER);
        init();
    }

    private void callAPI(String getNearbyCustomer) {


        Users user = prefHelper.getUser();
        Location location = user.getmCurrentLocation();


        Set<String> currentLocation = prefHelper.getCurrentLocation(context);
        if (currentLocation == null) {
            Utils.showLongToastInCenter(context, "Unable to get your current location. Please check your GPS Settings.");
            return;
        }
        List<String> locations = new ArrayList<>(currentLocation);
        Log.e("Nearby api hit ", "latitude " + locations.get(0) + " longitude " + locations.get(1));
        pDialog = Utils.showSweetLoader(context, SweetAlertDialog.PROGRESS_TYPE, "Fetching Customers");
        //Log.e(TAG, "Current Location" + location.getLatitude());

       /* Call<WebResponse<CustomerBody>> call = WebServiceFactory.getInstance().getNearByCustomers(getNearbyCustomer,
                prefHelper.getUser().getUserId(), String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));*/
        Call<WebResponse<CustomerBody>> call = WebServiceFactory.getInstance().getNearByCustomers(getNearbyCustomer,
                prefHelper.getUser().getUserId(), locations.get(1), locations.get(0));
//        "24.9499933", "67.0181618"
        call.enqueue(new Callback<WebResponse<CustomerBody>>() {
            @Override
            public void onResponse(Call<WebResponse<CustomerBody>> call, Response<WebResponse<CustomerBody>> response) {
                Utils.hideSweetLoader(pDialog);
                if (response.body() == null)
                    Utils.showShortToastInCenter(context, "Found null in web response");
                else if (response.body().isSuccess() && response.body().getBody().getCustomers().size() > 0) {
                    arrayList = response.body().getBody().getCustomers();
                    mAdapter.notifyDataSetChanged();
                } else {
                    findViewById(R.id.tv_nocustomerfound).setVisibility(View.VISIBLE);
                    Log.e(TAG, response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<CustomerBody>> call, Throwable t) {
                Utils.hideSweetLoader(pDialog);
                Utils.showShortToastInCenter(context, "something went wrong, please try again");
                Log.e(TAG, t.getMessage());

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {

        tv_Warnning = (TextView) findViewById(R.id.tv_warnning);
        rv_RecyclerView = (RecyclerView) findViewById(R.id.rv_nearbycustomers_recycler_view);
        mAdapter = new NearbyCustomersAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_RecyclerView.setLayoutManager(mLayoutManager);
        rv_RecyclerView.setItemAnimator(new DefaultItemAnimator());
        rv_RecyclerView.setAdapter(mAdapter);
        if (Utils.isNetworkConnected(context)) {
            rv_RecyclerView.setVisibility(View.VISIBLE);
            tv_Warnning.setVisibility(View.GONE);

        } else {
            tv_Warnning.setVisibility(View.VISIBLE);
            rv_RecyclerView.setVisibility(View.GONE);
        }
    }


    private class NearbyCustomersAdapter extends RecyclerView.Adapter<NearbyCustomersAdapter.VH> {

        Context context;

        public NearbyCustomersAdapter(Context context) {
            this.context = context;
        }

        @Override
        public NearbyCustomersAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nearbycustomers_recyclerview, parent, false);
            return new VH(v);

        }

        @Override
        public void onBindViewHolder(NearbyCustomersAdapter.VH holder, final int position) {
            holder.tv_Name.setText("Name: " + arrayList.get(position).getCustomerName());
            holder.tv_Number.setText("Phone: " + arrayList.get(position).getContactNumber());
            if (arrayList.get(position).getDistanceDifference() != null && !String.valueOf(arrayList.get(position).getDistanceDifference()).isEmpty())
                holder.tvDistance.setText(arrayList.get(position).getDistanceDifference() + " KM");
            else
                holder.tvDistance.setText("N/A");

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, CheckInFormActivity.class).putExtra("number", arrayList.get(position).getContactNumber()).putExtra("type", "nearby"));
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class VH extends RecyclerView.ViewHolder {
            TextView tv_Name, tv_Number, tvDistance;

            public VH(View itemView) {
                super(itemView);
                tv_Name = (TextView) itemView.findViewById(R.id.tv_name);
                tv_Number = (TextView) itemView.findViewById(R.id.tv_number);
                tvDistance = (TextView) itemView.findViewById(R.id.tv_distance);

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
