package hyenoon.com.sfts.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.widget.EditText;
import android.widget.TextView;

import hyenoon.com.sfts.R;

/**
 * Created by Dev.Arsalan on 2/1/2017.
 */

public class TextUtil {
    /**
     * @param textView widget in which the the to be set
     * @param string   value to set in textView
     * @action if string is null/empty
     * then textView would set null
     * as text else the value of string
     */
    public static void setText(@NonNull TextView textView, @Nullable String string) {
        if (!string.isEmpty()) {
//            String upperString = string.substring(0,1).toUpperCase() + string.substring(1);
            String upperString = string;
            textView.setText(upperString);
        } else textView.setText("");
    }

    private SpannableString setLastTextBold(Context context, String s1, String s2) {

        SpannableString str = new SpannableString(s1 + "\n" + s2);
        int len = s1.length() + s2.length();
        str.setSpan(new AbsoluteSizeSpan(30), s1.length(), len + 1, 0);

        str.setSpan(new StyleSpan(Typeface.BOLD), s1.length(), len + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.PurpleButton)), s1.length(), len + 1, 0);
        return str;
    }

    public static SpannableString setStartTextBold(Context context, String s1, String s2, int color,
                                                   int boldTextSize, int startLen, int endLen, boolean nextLine) {
        SpannableString str;
        if (nextLine) {
            str = new SpannableString(s1 + "\n" + s2);
        } else
            str = new SpannableString(s1 + " " + s2);
//        int len = s1.length() + s2.length();
        str.setSpan(new AbsoluteSizeSpan(boldTextSize), 0, s1.length(), 0);

        str.setSpan(new StyleSpan(Typeface.BOLD), startLen, endLen, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        str.setSpan(new ForegroundColorSpan(color), startLen, endLen, 0);
        return str;
    }

    public static void setMaxLength(Context context, EditText editText, int length) {
//        InputFilter[] FilterArray = new InputFilter[1];
//        FilterArray[0] = new InputFilter.LengthFilter(length);
//        editText.setFilters(FilterArray);

        editText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(length)
        });
    }

}
