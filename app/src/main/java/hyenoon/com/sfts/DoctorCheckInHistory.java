package hyenoon.com.sfts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hyenoon.com.sfts.Adapters.CheckInHistoryListAdapter;
import hyenoon.com.sfts.Adapters.CheckInListAdapter;
import hyenoon.com.sfts.Entities.DoctorCheckInItem;
import hyenoon.com.sfts.Fragments.DoctorCheckIn;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.Utils.DateUtils;
import hyenoon.com.sfts.Utils.Utils;

public class DoctorCheckInHistory extends BaseActivity {

    private BasePreferenceHelper prefHelper;
    private CheckInHistoryListAdapter mCheckInHistoryListAdapter;
    private static String dateFormat = "dd MMM yyyy HH:mm:ss";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_check_in_history);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Today's Calls");
        }

        prefHelper = new BasePreferenceHelper(this);

        final ListView listview = (ListView)findViewById(R.id.listview_history);
        mCheckInHistoryListAdapter = new CheckInHistoryListAdapter(this, getTodayVisitedDoctors());
        listview.setAdapter(mCheckInHistoryListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

//        menuInflater.inflate(R.menu.mainmenu, menu);

//        int badgeCount = SharedPreference.getInstance(this).getSyncRequestCount();
//        itemSync = menu.findItem(R.id.item_samplebadge);
//        ActionItemBadge.update(this, itemSync, FontAwesome.Icon.faw_refresh, ActionItemBadge.BadgeStyles.RED, badgeCount);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected Boolean checkForTodayDate(String dateTime) {
        boolean isToday = false;
        Date dateVisited = Utils.stringToDate(dateTime, dateFormat);
        if (DateUtils.isToday(dateVisited))
            isToday = true;

        return isToday;
    }

    public ArrayList<DoctorCheckInItem> getTodayVisitedDoctors() {

        ArrayList<DoctorCheckInItem> listDoctors = new ArrayList<>();

        if (prefHelper.getSavedDoctorCheckInList() != null
                && !prefHelper.getSavedDoctorCheckInList().isEmpty()) {
            ArrayList<DoctorCheckInItem> listCheckedInDoctors = prefHelper.getSavedDoctorCheckInList();
            for (DoctorCheckInItem item:
                    listCheckedInDoctors) {
                String visitedDate = Utils.epochDateToString(item.getCurrentTime(), dateFormat);
                if (checkForTodayDate(visitedDate))
                    listDoctors.add(item);
            }
        }

        return listDoctors;
    }
}
