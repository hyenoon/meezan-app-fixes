package hyenoon.com.sfts.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import hyenoon.com.sfts.Entities.ProductLOV;
import hyenoon.com.sfts.NearbyCustomersActivity;
import hyenoon.com.sfts.R;
import hyenoon.com.sfts.ServiceModel.GsonFactory;

/**
 * Created by us on 3/2/2018.
 */

public class Dialogfragment extends DialogFragment {

    RecyclerView myRecyclerView;
    MyDialogFragmentAdapter adapter;
    Context context;
    List<ProductLOV> lovList;
    Button btn_Submit;
    boolean[] selectedPosition;

    public Dialogfragment.sumitClick getSumitClick() {
        return sumitClick;
    }

    public void setSumitClick(Dialogfragment.sumitClick sumitClick) {
        this.sumitClick = sumitClick;
    }

    sumitClick sumitClick;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialogfragment, container,
                false);
        getDialog().setTitle("Dialogfragment Tutorial");
        setCancelable(false);
        Bundle bundle = getArguments();
        lovList = getDoctorListFromJson(bundle.getString("json"));
        Log.e("DialogFragmetn","Size "+lovList.size());
        selectedPosition = new boolean[lovList.size()];
        selectedPosition = getArguments().getBooleanArray("selectedPosition");
        // Log.e("selectedPosition","selectedPosition "+selectedPosition.length);

        context = getActivity();
        Log.e("Dialogfragment", "size " + lovList.size());

        myRecyclerView = (RecyclerView) rootView.findViewById(R.id.mRecyerID);
        adapter = new MyDialogFragmentAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        myRecyclerView.setLayoutManager(mLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(adapter);
        adapter.setListener((clickListener) context);

        btn_Submit = (Button) rootView.findViewById(R.id.btn_submit);
        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sumitClick != null) {
                    sumitClick.submit();
                    Log.e("onBind if check ", " Called");

                }
                dismiss();
            }
        });

        // Do something else
        return rootView;
    }

    private List<ProductLOV> getDoctorListFromJson(String json) {
        Type collectionType = new TypeToken<List<ProductLOV>>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    class MyDialogFragmentAdapter extends RecyclerView.Adapter<VH> {

        public void setListener(clickListener listener) {
            this.listener = listener;
        }

        clickListener listener;

        public MyDialogFragmentAdapter() {

        }

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_spinner, parent, false);
            return new VH(v);
        }

        @Override
        public void onBindViewHolder(final VH holder, final int position) {

            Log.e("Check or Not", "Status " + lovList.get(position).isCheckedStatus());
            Log.e("Product Name"," : "+lovList.get(position).getProductName());
            holder.checkBox.setText(lovList.get(position).getProductName());
            if (selectedPosition[position])
                holder.checkBox.setChecked(true);
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.e("onBindViewHolder", " Called");
                    if (isChecked) {
                        if (listener != null) {

                            listener.postion(isChecked, position);
                            //  lovList.get(selectedPosition).setCheckedStatus(true);
                            Log.e("onBind if check ", " Called");

                        }
                    } else {
                        if (listener != null) {

                            listener.postion(false, position);
                            //  lovList.get(selectedPosition).setCheckedStatus(true);
                            selectedPosition[position]=false;
                            lovList.get(position).setCheckedStatus(false);
                            Log.e("onBind if check ", " Called");

                        }

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return lovList.size();
        }


    }

    public class VH extends RecyclerView.ViewHolder {
        CheckBox checkBox;

        public VH(View itemView) {
            super(itemView);
            checkBox = (CheckBox) itemView.findViewById(R.id.cbx_serveform_selectionbox);

        }
    }

    public interface clickListener {
        void postion(boolean isChecked, int postion);
    }

    public interface sumitClick {
        void submit();
    }

}
