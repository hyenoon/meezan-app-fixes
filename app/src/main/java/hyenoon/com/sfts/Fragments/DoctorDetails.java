package hyenoon.com.sfts.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import hyenoon.com.sfts.BaseActivity;
import hyenoon.com.sfts.CheckInFormActivity;
import hyenoon.com.sfts.Entities.DetailsItems;
import hyenoon.com.sfts.Entities.Doctor;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.Location.SimpleLocation;
import hyenoon.com.sfts.R;
import hyenoon.com.sfts.ServiceModel.GsonFactory;
import hyenoon.com.sfts.Services.SyncService;
import hyenoon.com.sfts.Utils.Utils;

public class DoctorDetails extends Fragment implements View.OnClickListener {
    private static Doctor Detaildoctor;
    String TAG = "DoctorDetails";
    private static final String ARGUMENT_DOCTOR = "doctor";
    private BasePreferenceHelper prefHelper;
    private Doctor doctor;
    ArrayList<DetailsItems> detailsItems = new ArrayList<>();
    RecyclerView r_RecyclerView;
    CustomerDetailAdapter detailAdapter;
    ArrayList<String> jsonKey = new ArrayList<>();
    ArrayList<String> jsonValue = new ArrayList<>();
    private Menu menu;
    FloatingActionButton fabCall;
    private DoctorDetailsFragmentListener mListener;
    private Context context;
    private final int CALL_REQUEST = 222;

    private List<String> detailsArray = new ArrayList<>();


    public DoctorDetails() {
        // Required empty public constructor
    }

    public static DoctorDetails newInstance(Doctor doctor) {
        Detaildoctor = doctor;
        final DoctorDetails fragment = new DoctorDetails();

        final Bundle args = new Bundle();
        args.putString(ARGUMENT_DOCTOR, new Gson().toJson(doctor));
        fragment.setArguments(args);

        return fragment;
    }

    public void onUpdateDoctorLocation() {

        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        if (mLocation.hasLocationEnabled()
                && (mLocation.getLatitude() != 0 || mLocation.getLongitude() != 0)) {
            updateDoctorLocation(mLocation.getLatitude(), mLocation.getLongitude());
        } else if (prefHelper.getUser().getmCurrentLocation() != null) {
            Location location = prefHelper.getUser().getmCurrentLocation();
            updateDoctorLocation(location.getLatitude(), location.getLongitude());
        } else {
            updateDoctorLocation(0, 0);
        }
    }

    public void updateDoctorLocation(double latitude, double longitude) {
        Doctor _doctor = this.doctor;
        _doctor.setLatitude(String.valueOf(latitude));
        _doctor.setLongitude(String.valueOf(longitude));

        //  save update doctor request in cache.
        ArrayList<Doctor> doctors = prefHelper.getUpdatedDoctorsList();
        if (doctors == null)
            doctors = new ArrayList<Doctor>();

        doctors.add(_doctor);
        prefHelper.putUpdateDoctorList(doctors);

        BaseActivity baseActivity = (BaseActivity) getActivity();
        baseActivity.stopServiceClass(SyncService.class); // it will restart service in 10secs

        Utils.showShortToastInCenter(getActivity(), "Customer's location updated successfully");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        // make the device update its location
        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        mLocation.beginUpdates();
    }

    @Override
    public void onPause() {
        // stop location updates (saves battery)
        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        mLocation.endUpdates();

        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        Log.e("onCreateView", "Called");

        View view = inflater.inflate(R.layout.fragment_doctor_details, container, false);

        init(view);
        final Bundle args = getArguments();
        String jsonDoctor = args.getString(ARGUMENT_DOCTOR);
        doctor = new Gson().fromJson(jsonDoctor, Doctor.class);
        updateAdapter(doctor.getDetails());

//        this.doctor = new Gson().fromJson(jsonDoctor, Doctor.class);
//        Log.e(TAG, "Contact " + this.doctor.getContactNumber());

//        JSONObject resobj = null;
//        try {
//
//            resobj = new JSONObject(new Gson().toJson(doctor.getDetails()));
//            Log.e(TAG, "jsonObj " + resobj);
//            Iterator<?> keys = resobj.keys();
//            Log.e(TAG, "keys.hasNext() " + keys.hasNext());
//
//            while (keys.hasNext()) {
//                String key = (String) keys.next();
//                Log.e(TAG, "Key Data" + key);
//                Log.e(TAG, "Value " + resobj.getString(key));
//                detailsItems.add(new DetailsItems(key, resobj.getString(key)));
//
//                /*if ( resobj.get(key) instanceof JSONObject ) {
//                    JSONObject xx = new JSONObject(resobj.get(key).toString());
//                   Log.e("Key",xx.getString(key));
//                    detailsItems.add(new DetailsItems(key,xx.getString(key)));
//                    *//*jsonKey.add(key);
//                    jsonValue.add(xx.getString(key));*//*
//                }*/
//            }
//            Log.e("Try", "called");
//            Log.e("Detials Size", "Size " + detailsItems.size());
//            detailAdapter.notifyDataSetChanged();
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            Log.e("Catch", "called");
//        }


        return view;
    }

    private void updateAdapter(List<String> details) {
        if (details != null && details.size() > 0) {
            detailsArray.addAll(details);
            detailAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        // TODO Add your menu entries here
        inflater.inflate(R.menu.chechinmenu, menu);
        menu.findItem(R.id.action_home).setVisible(false);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        Intent intent = new Intent(getActivity(), CheckInFormActivity.class);
        intent.putExtra("number", Detaildoctor.getContactNumber());
        intent.putExtra("type", "checkin");

        startActivity(intent);
        return false;


    }

    private Doctor getCustomerFromJson(String json) {
        Type collectionType = new TypeToken<Doctor>() {
        }.getType();
        return GsonFactory.getConfiguredGson().fromJson(json, collectionType);
    }

    private void init(View view) {
        Log.e("init", "called");
        fabCall = (FloatingActionButton) view.findViewById(R.id.fab_call);
        fabCall.setOnClickListener(this);
        r_RecyclerView = (RecyclerView) view.findViewById(R.id.rv_customerdetail_recyclerview);
        detailAdapter = new CustomerDetailAdapter();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        r_RecyclerView.setLayoutManager(mLayoutManager);
        r_RecyclerView.setItemAnimator(new DefaultItemAnimator());
        r_RecyclerView.setAdapter(detailAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        prefHelper = new BasePreferenceHelper(getActivity());

        if (context instanceof DoctorDetailsFragmentListener) {
            mListener = (DoctorDetailsFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement DoctorDetailsFragmentListener.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fab_call) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, CALL_REQUEST);
                        return;
                    }
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);

                callIntent.setData(Uri.parse("tel:" + this.doctor.getContactNumber()));

                startActivity(callIntent);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


    }

    public interface DoctorDetailsFragmentListener {
        void onDoctorCheckIn(Doctor doctor);
    }

    class CustomerDetailAdapter extends RecyclerView.Adapter<CustomerDetailAdapter.VH> {
        Context context;

        public CustomerDetailAdapter() {

        }

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_customerdetails_recyclerview, parent, false);
            return new VH(v);
        }

        @Override
        public void onBindViewHolder(VH holder, int position) {
            Log.e("onBindViewHolder", "called");
            holder.tv_Key.setText(detailsArray.get(position));
//            holder.tv_Key.setText(detailsItems.get(position).getKew());
//            holder.tv_Value.setText(detailsItems.get(position).getValue());

        }

        @Override
        public int getItemCount() {
//            return detailsItems.size();
            return detailsArray.size();
        }

        public class VH extends RecyclerView.ViewHolder {
            TextView tv_Key, tv_Value;

            public VH(View itemView) {
                super(itemView);
                tv_Key = (TextView) itemView.findViewById(R.id.tv_customerdetail_key);
                tv_Value = (TextView) itemView.findViewById(R.id.tv_customerdetail_value);
            }
        }
    }
}
