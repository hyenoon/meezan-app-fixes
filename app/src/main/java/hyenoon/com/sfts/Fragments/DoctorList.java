package hyenoon.com.sfts.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import hyenoon.com.sfts.Entities.Doctor;
import hyenoon.com.sfts.Entities.ExtraCustomerDetails;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.R;

public class DoctorList extends Fragment implements View.OnClickListener, SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private SearchView searchView;

    private ArrayList<Doctor> listDoctors;
    private DoctorListAdapter doctorListAdapter;
    private BasePreferenceHelper prefHelper;
    private OnDoctorListFragmentListener mListener;
    private ExtraCustomerDetails extraDetailCistomers;
    Button btnAddTeamLead;

    public DoctorList() {
        // Required empty public constructor
    }

    public static DoctorList newInstance() {
        DoctorList fragment = new DoctorList();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefHelper = new BasePreferenceHelper(getActivity());
//        if (prefHelper.getDoctorsList() != null && prefHelper.getDoctorsList().size() > 0) {
//            listDoctors = prefHelper.getDoctorsList();
//
////            extraDetailCistomers=prefHelper.getDoctorsList().get(0).getDetails();
//        } else {
//            listDoctors = new ArrayList<>();
//        }
        if (prefHelper.getCustomersList() != null && prefHelper.getCustomersList().size() > 0) {
            listDoctors = prefHelper.getCustomersList();

//            extraDetailCistomers=prefHelper.getDoctorsList().get(0).getDetails();
        } else {
            listDoctors = new ArrayList<>();
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_doctor_list, container, false);

        searchView = (SearchView) view.findViewById(R.id.searchDoctor);
        searchView.setOnQueryTextListener(this);

        final Activity activity = getActivity();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleView);
        recyclerView.setLayoutManager(layoutManager);
        doctorListAdapter = new DoctorListAdapter(activity);
        Log.e("DoctorList", "doctor onCreateView" + Arrays.toString(listDoctors.toArray()));
        recyclerView.setAdapter(doctorListAdapter);

        final Button btnAddDoctor = (Button) view.findViewById(R.id.btnAddTeamlead);
        btnAddDoctor.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnDoctorListFragmentListener) {
            mListener = (OnDoctorListFragmentListener) context;
        } else {
            throw new ClassCastException(context.toString() + "must implement OnDoctorSelected.");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddTeamlead:
                mListener.onAddDoctor();
                break;
        }
    }

    @Override
    public boolean onQueryTextChange(String query) {
        doctorListAdapter.filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        doctorListAdapter.filter(query);
        return true;
    }

    class DoctorListAdapter extends RecyclerView.Adapter<ViewHolder> {

        private LayoutInflater mLayoutInflater;
        private ArrayList<Doctor> filterDoctorList;

        public DoctorListAdapter(Context context) {
            Log.e("DoctorList", "doctor DoctorListAdapter" + Arrays.toString(listDoctors.toArray()));
            mLayoutInflater = LayoutInflater.from(context);
            filterDoctorList = new ArrayList<>(listDoctors);
        }

        public void filter(String text) {
            if (text.isEmpty()) {
                filterDoctorList.clear();
                filterDoctorList.addAll(listDoctors);
            } else {
                ArrayList<Doctor> result = new ArrayList<>();
                text = text.toLowerCase();
                for (Doctor item : listDoctors) {
                    if (item.getCustomerName().toLowerCase().contains(text)) {
                        result.add(item);
                    }
                }
                filterDoctorList.clear();
                filterDoctorList.addAll(result);
            }
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            return new ViewHolder(mLayoutInflater
                    .inflate(R.layout.recycler_item_doctor_list, viewGroup, false));
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            final Doctor doctor = filterDoctorList.get(position);
            final String doctorName = doctor.getCustomerName();
            final String speciality = doctor.getContactNumber();
            final String description = doctor.getAddress();
            viewHolder.setData(doctorName, speciality, description);

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onDoctorSelected(doctor);
                }
            });
        }

        @Override
        public int getItemCount() {
            return filterDoctorList.size();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        // Views
        private TextView txtTitle;
        private TextView txtDate;
        private TextView txtDesc;
        private ImageView itemTick;
        private RelativeLayout containerItem;

        private ViewHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
            itemTick = (ImageView) itemView.findViewById(R.id.imgTick);
            containerItem = (RelativeLayout) itemView.findViewById(R.id.containerItem);
        }

        private void setData(String doctorName, String speciality, String description) {
            txtTitle.setText(doctorName);
            txtDate.setText(speciality);
            txtDesc.setText(description);
        }
    }

    public interface OnDoctorListFragmentListener {
        void onDoctorSelected(Doctor doctor);

        void onAddDoctor();
    }
}
