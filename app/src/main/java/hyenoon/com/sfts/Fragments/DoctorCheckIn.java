package hyenoon.com.sfts.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import hyenoon.com.sfts.Adapters.CheckInListAdapter;
import hyenoon.com.sfts.BaseActivity;
import hyenoon.com.sfts.Entities.Doctor;
import hyenoon.com.sfts.Entities.DoctorCheckInItem;
import hyenoon.com.sfts.Entities.Product;
import hyenoon.com.sfts.Entities.Survey;
import hyenoon.com.sfts.Entities.SurveyProductItem;
import hyenoon.com.sfts.Entities.SurveyQuestionItem;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.Location.SimpleLocation;
import hyenoon.com.sfts.R;
import hyenoon.com.sfts.Services.SyncService;
import hyenoon.com.sfts.Utils.Utils;

public class  DoctorCheckIn extends Fragment implements View.OnClickListener {

    private static final String ARGUMENT_DOCTOR = "doctor";

    private BasePreferenceHelper prefHelper;
    private CheckInListAdapter listAdapter;
    private Doctor doctor;
    private Survey survey;

    public DoctorCheckIn() {
        // Required empty public constructor
    }

    public static DoctorCheckIn newInstance(Doctor doctor) {
        final DoctorCheckIn fragment = new DoctorCheckIn();

        final Bundle args = new Bundle();
        args.putString(ARGUMENT_DOCTOR, new Gson().toJson(doctor));
        fragment.setArguments(args);

        return fragment;
    }

    public void onAddCheckInProduct() {

        if (survey.getProducts().size() > 0) {
            Product product = survey.getProducts().get(0);

            SurveyProductItem surveyProductItem = new SurveyProductItem();
            surveyProductItem.setSelectedProduct(product.getName());
            surveyProductItem.setSelectedcompetitiveProduct(product.getCompetitiveProduct().get(0));

            listAdapter.addItem(surveyProductItem);
        }
    }

    public void onCheckedInDoctor(Boolean surveyDetails) {

        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        Log.e("check in", "Lat=> " + mLocation.getLatitude() + " Long=> " + mLocation.getLongitude());
        if (!mLocation.hasLocationEnabled()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
            dialog.setMessage("Please turn on GPS in settings");
            dialog.setTitle("Warning");

            dialog.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SimpleLocation.openSettings(getContext());
                    dialog.dismiss();
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.show();

            return;
        }
        if (mLocation.hasLocationEnabled()
                && (mLocation.getLatitude() != 0 || mLocation.getLongitude() != 0)) {
            addCheckInDoctor(mLocation.getLatitude(), mLocation.getLongitude(), surveyDetails);
        } else if (prefHelper.getUser().getmCurrentLocation() != null) {
            Location location = prefHelper.getUser().getmCurrentLocation();
            if (location.getLatitude() == 0 && location.getLongitude() == 0)
                Utils.showLongToastInCenter(getActivity(), R.string.locationZeroFound);
            addCheckInDoctor(location.getLatitude(), location.getLongitude(), surveyDetails);


        } else {
            Utils.showLongToastInCenter(getActivity(), R.string.locationZeroFound);
            addCheckInDoctor(0, 0, surveyDetails);


        }
    }

    public void addCheckInDoctor(double latitude, double longitude, Boolean surveyDetails) {

        // Check-in doctors without survey details.
        DoctorCheckInItem checkInItem = new DoctorCheckInItem();
        checkInItem.setDoctor(doctor);
        checkInItem.setDoctorId(doctor.getCustomerId());
        checkInItem.setUserId(prefHelper.getUser().getUserId());
        checkInItem.setLatitude(String.valueOf(latitude));
        checkInItem.setLongitude(String.valueOf(longitude));
        checkInItem.setCurrentTime(System.currentTimeMillis());

        if (surveyDetails == true) {

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

            // Add survey questions
            ArrayList<SurveyQuestionItem> questions = listAdapter.getQuestions();
            String jsonQuestions = gson.toJson(questions);
            checkInItem.setQuestions(jsonQuestions);

            // Add survey programs
            ArrayList<SurveyProductItem> products = listAdapter.getProducts();
            if (products.size() > 0) {
                String jsonProducts = gson.toJson(products);
                checkInItem.setProducts(jsonProducts);
            }
        }

        // List of doctors checked-in to be sync with web.
        ArrayList<DoctorCheckInItem> listCheckInDoctors = prefHelper.getDoctorCheckInList();
        if (listCheckInDoctors == null)
            listCheckInDoctors = new ArrayList<DoctorCheckInItem>();
        listCheckInDoctors.add(checkInItem);
        prefHelper.putDoctorCheckInList(listCheckInDoctors);

        // List of doctors checked-in to be saved for history.
        ArrayList<DoctorCheckInItem> listSavedCheckInDoctors = prefHelper.getSavedDoctorCheckInList();
        if (listSavedCheckInDoctors == null)
            listSavedCheckInDoctors = new ArrayList<DoctorCheckInItem>();
        listSavedCheckInDoctors.add(checkInItem);
        prefHelper.putSavedDoctorCheckInList(listSavedCheckInDoctors);

        BaseActivity baseActivity = (BaseActivity) getActivity();
        baseActivity.stopServiceClass(SyncService.class); // it will restart service in 10 secs

        Utils.showShortToastInCenter(getActivity(), "Customer checked-in successfully");
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // make the device update its location
        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        mLocation.beginUpdates();
    }

    @Override
    public void onPause() {
        // stop location updates (saves battery)
        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        mLocation.endUpdates();
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctor_check_in, container, false);

        final Bundle args = getArguments();
        String jsonDoctor = args.getString(ARGUMENT_DOCTOR);
        this.doctor = new Gson().fromJson(jsonDoctor, Doctor.class);

        final ListView listview = (ListView)view.findViewById(R.id.listView);
        listAdapter = new CheckInListAdapter(getActivity(), survey);
        listview.setAdapter(listAdapter);

        LinearLayout footerLayout = (LinearLayout)getActivity().getLayoutInflater().inflate(R.layout.survey_item_button, null);
        Button btnAddProduct = (Button) footerLayout.findViewById(R.id.btn_survey_item_add_product);
        btnAddProduct.setOnClickListener(this);
        listview.addFooterView(footerLayout);

        final Button btnSubmit = (Button)view.findViewById(R.id.btn_check_in_submit);
        btnSubmit.setOnClickListener(this);

        final Button btnSkip = (Button)view.findViewById(R.id.btn_check_in_skip);
        btnSkip.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        prefHelper = new BasePreferenceHelper(getActivity());
        Survey objSurvey = prefHelper.getSurveyDetails();
        if (objSurvey == null) {
            // Survey not found
            return;
        }

        this.survey = objSurvey;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_check_in_submit:
                onCheckedInDoctor(true);
                break;
            case R.id.btn_check_in_skip:
                onCheckedInDoctor(false);
                break;
            case R.id.btn_survey_item_add_product:
                //Add product item to list
                onAddCheckInProduct();
                break;
        }
    }
}