package hyenoon.com.sfts.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import hyenoon.com.sfts.CheckInFormActivity;
import hyenoon.com.sfts.Entities.Area;
import hyenoon.com.sfts.Entities.Speciality;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.Location.SimpleLocation;
import hyenoon.com.sfts.R;
import hyenoon.com.sfts.Utils.Utils;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class AddDoctor extends Fragment implements View.OnClickListener {

    EditText editTextDoctor;
    EditText editTextClinicName;
    TextView editTextAddress;
    EditText editTextClass;
    EditText editTextContact;

    ArrayAdapter<Speciality> adapterSpecial;
    ArrayAdapter<Area> adapterAreaCode;
    String selectedAreaCode, selectedSpec = "";
    private BasePreferenceHelper prefHelper;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    LatLng mLatLng;

    public AddDoctor() {
        // Required empty public constructor
    }

    public static AddDoctor newInstance() {
        AddDoctor fragment = new AddDoctor();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // make the device update its location
        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        mLocation.beginUpdates();
    }

    @Override
    public void onPause() {
        // stop location updates (saves battery)
        SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
        mLocation.endUpdates();

        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_add_doctor, container, false);

        final Button btnAddDoctor = (Button)view.findViewById(R.id.btnSubmit);
        btnAddDoctor.setOnClickListener(this);

        editTextDoctor = (EditText)view.findViewById(R.id.edtTxtDoctorName);
        editTextClinicName = (EditText)view.findViewById(R.id.edtTxtClinicName);
        editTextAddress = (TextView)view.findViewById(R.id.edtTxtAddress);
        editTextClass = (EditText)view.findViewById(R.id.edtTxtEmal);
        editTextContact = (EditText)view.findViewById(R.id.edtTxtContact);
        editTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Clicked","clicked");
                searchAddress();
            }
        });
//
//        addItemsOnSpecialitySpinner(view);
//        addItemsOnAreaCodeSpinner(view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        prefHelper = new BasePreferenceHelper(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                onClickSubmit(view);
                break;

        }
    }

        private void searchAddress() {

            try {

                AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                        .build();

                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                .setFilter(typeFilter)
                                .build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

            } catch (GooglePlayServicesRepairableException e) {
                // TODO: Handle the error.
            } catch (GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.

            }

        }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                editTextAddress.setText(place.getAddress());
                mLatLng=place.getLatLng();
                Log.e("called ", (String) place.getAddress());
                Log.e("LatLong : ", String.valueOf(mLatLng));
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

            } else if (resultCode == RESULT_CANCELED) {
//                finish();
                // The user canceled the operation.
            }
        }

    }

    /*public void addItemsOnSpecialitySpinner(View rootView) {

            spinnerSpeciality = (Spinner)rootView.findViewById(R.id.spinnerSpeciality);
            if (prefHelper.getSpecialityList() != null
                    && !prefHelper.getSpecialityList().isEmpty()) {

                adapterSpecial = new ArrayAdapter<Speciality>(
                        getContext(), android.R.layout.simple_spinner_dropdown_item, prefHelper.getSpecialityList());
                spinnerSpeciality.setAdapter(adapterSpecial);
            }
            else {
                // TODO get_speciality webservice should be called.
            }

            spinnerSpeciality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Speciality speciality = (Speciality)parent.getItemAtPosition(position);
                    selectedSpec = speciality.getSpecialityId();
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        public void addItemsOnAreaCodeSpinner(View rootView) {

            spinnerAreaCode = (Spinner)rootView.findViewById(R.id.spinnerAreaCode);
            if (prefHelper.getAreaCodeList() != null
                    && !prefHelper.getAreaCodeList().isEmpty()) {

                adapterAreaCode = new ArrayAdapter<Area>(
                        getContext(), android.R.layout.simple_spinner_dropdown_item, prefHelper.getAreaCodeList());
                spinnerAreaCode.setAdapter(adapterAreaCode);
            }
            else {
                // TODO get_area webservice should be called.
            }

            spinnerAreaCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Area area = (Area) parent.getItemAtPosition(position);
                    selectedAreaCode = area.getAreaId();
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    */
/*

        //In Pervious code,getting lat and long of current locqation and then passing to the time of adding doctor.
    public void onClickSubmit(View view) {
        if (!Utils.isEmptyOrNull(editTextDoctor.getText().toString())
                && !Utils.isEmptyOrNull(editTextAddress.getText().toString())
                && !Utils.isEmptyOrNull(editTextClinicName.getText().toString())
                && !Utils.isEmptyOrNull(editTextClass.getText().toString())) {

            SimpleLocation mLocation = SimpleLocation.getInstance(getContext());
            if (mLocation.hasLocationEnabled()
                    && (mLocation.getLatitude() != 0 || mLocation.getLongitude() != 0)) {
                addDoctor(mLocation.getLatitude(), mLocation.getLongitude());
            } else if (prefHelper.getUser().getmCurrentLocation() != null) {
                Location location = prefHelper.getUser().getmCurrentLocation();
                addDoctor(location.getLatitude(), location.getLongitude());
            }
            else {
                addDoctor(0, 0);
            }
        }
        else {
            Utils.showLongToastInCenter(getContext(), "Required information not provided");
        }
    }
*/

    //In this method,avoiding to get lat long from current location instead of user type his address.

    public void onClickSubmit(View view) {
        if (!Utils.isEmptyOrNull(editTextDoctor.getText().toString())
                && !Utils.isEmptyOrNull(editTextAddress.getText().toString())
                && !Utils.isEmptyOrNull(editTextClinicName.getText().toString())
                && !Utils.isEmptyOrNull(editTextClass.getText().toString())) {

            addDoctor(mLatLng);

        }
        else {
            Utils.showLongToastInCenter(getContext(), "Required information not provided");
        }
    }


    public void addDoctor(LatLng mLatlong) {


        Intent intent=new Intent(getActivity(), CheckInFormActivity.class);
        intent.putExtra("name",editTextDoctor.getText().toString());
        intent.putExtra("number",editTextContact.getText().toString());
        intent.putExtra("companyName",editTextClinicName.getText().toString());
        intent.putExtra("address",editTextAddress.getText().toString());
        intent.putExtra("emailAddress",editTextClass.getText().toString());
        intent.putExtra("type","addlead");
        startActivity(intent);


     /*   Doctor doctor = new Doctor();

        doctor.setCustomerId(prefHelper.getUser().getUserId());
        doctor.setCustomerName(editTextDoctor.getText().toString());
        doctor.setAddress(editTextClass.getText().toString());
        doctor.setContactNumber(editTextClinicName.getText().toString());
        doctor.setAddress(editTextAddress.getText().toString());
        doctor.setEmailAddress(selectedSpec);
        doctor.setContactNumber(editTextContact.getText().toString());
       // doctor.setAreaId(selectedAreaCode);
        doctor.setLatitude(String.valueOf(mLatlong.latitude));
        doctor.setLongitude(String.valueOf(mLatlong.longitude));
      //  doctor.setCurrentTime(System.currentTimeMillis());

        // do add doctor request in cache.
        ArrayList<Doctor> doctors = prefHelper.getAddedDoctorsList();
        if (doctors == null)
            doctors = new ArrayList<Doctor>();

        doctors.add(doctor);

        prefHelper.putAddDoctorList(doctors);

        BaseActivity baseActivity = (BaseActivity) getActivity();
        baseActivity.stopServiceClass(SyncService.class); // it will restart service in 10secs

        Utils.showShortToastInCenter(getActivity(), "Team Lead added successfully");
        getActivity().getSupportFragmentManager().popBackStack();*/
    }
}
