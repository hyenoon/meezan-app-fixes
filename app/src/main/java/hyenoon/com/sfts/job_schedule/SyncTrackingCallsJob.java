package hyenoon.com.sfts.job_schedule;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.UserLocation;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.hyenoon.carvertising.constant.NetworkConstant;
//import com.hyenoon.carvertising.model.UserLocation;
//import com.hyenoon.carvertising.network.WebServiceFactory;
//import com.hyenoon.carvertising.pojo.BaseResponse;
//import com.hyenoon.carvertising.preference.BasePreferenceHelper;
//import com.hyenoon.carvertising.util.LogUtil;

/**
 * Created by Dev.Arsalan on 5/23/2017.
 */

public class SyncTrackingCallsJob extends Job {
    public static final String TAG = "sync_job_tag";
    private static final int MAX_COUNT = 100;

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        return performSyncTask(getContext());
    }

    public static void scheduleJob() {
        new JobRequest.Builder(SyncTrackingCallsJob.TAG)
                .setPersisted(true)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                .setUpdateCurrent(true)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
    }

    private Result performSyncTask(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        ArrayList<UserLocation> syncCallsList = preferenceHelper.getUserLocationList();
        if (syncCallsList != null
                && !syncCallsList.isEmpty()) {
            final ArrayList<UserLocation> listUserLocations = new ArrayList<>();
            int totalCount = (syncCallsList.size() < MAX_COUNT) ? syncCallsList.size() : MAX_COUNT;
            for (int index = 0; index < totalCount; index++) {
                listUserLocations.add(syncCallsList.get(index));
            }
            String jsonObj = new Gson().toJson(listUserLocations);
            Log.e(TAG, "performSyncTask *** JSON OBJECT TRACKING *** " + jsonObj);
            return sendDataToServer(Constants.kWebUpdateLocation, jsonObj, listUserLocations, preferenceHelper);
        } else {
            Log.e(TAG, "performSyncTask: *** No Location Call Found to Sync ***");
            return Result.SUCCESS;
        }
    }

    private Result sendDataToServer(String action, String jsonData, final ArrayList<UserLocation> listUserLocations, final BasePreferenceHelper preferenceHelper) {
        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().updateUserLocation(action, jsonData);
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body() == null || response.body().getHeader() == null) {
                    Log.e(TAG, "onResponse sendDataToServer: FOUND NULL OR EMPTY RESPONSE");
                } else if (response.body().getHeader().success.equals("1")) {
                    Log.e(TAG, "Successfully updated user locations: " + listUserLocations.size());
                    broadcastMissedCalls(true, preferenceHelper);
                    removeUserLocations(listUserLocations);
                } else {
                    Log.e(TAG, "onResponse sendDataToServer: " + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                Log.e(TAG, "Webservice failed: " + t.getLocalizedMessage());
            }
        });
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(10));
        Log.d(TAG, "RESULT Return");
        return Result.SUCCESS;
    }

    private void removeUserLocations(ArrayList<UserLocation> listUserLocations) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(getContext());
        ArrayList<UserLocation> locations = preferenceHelper.getUserLocationList();
        if (locations != null
                && !locations.isEmpty()) {
            if (listUserLocations != null) {
                for (int x = 0; x < listUserLocations.size(); x++) {
                    UserLocation userLocation = listUserLocations.get(x);
                    for (int y = 0; y < locations.size(); y++) {
                        UserLocation location = locations.get(y);
                        if (userLocation.getLocationTime() == location.getLocationTime()) {
                            locations.remove(y);
                            break;
                        }
                    }
                }
//                sendDebugData(locations.size());
                preferenceHelper.putUserLocationList(locations);
            }
        }
    }

    private void broadcastMissedCalls(boolean isSuccess, BasePreferenceHelper prefHelper) {
        if (isSuccess) {
            prefHelper.putMissedSynCalls(0);
        } else {
            int noOfMissedCalls = prefHelper.getMissedSynCalls();
            if (noOfMissedCalls == -1)
                noOfMissedCalls = 0;
            prefHelper.putMissedCheckinCalls(++noOfMissedCalls);
        }

        Intent intent = new Intent(Constants.kUIACTION);
        intent.putExtra(Constants.kACTION, "trackingCalls");
        intent.putExtra(Constants.kNoOfCheckinCalls, prefHelper.getMissedSynCalls());
        Utils.triggerBroadcast(getContext(), intent);
    }
}