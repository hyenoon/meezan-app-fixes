package hyenoon.com.sfts.job_schedule;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by Dev.Arsalan on 5/23/2017.
 */

public class SyncCallsJobCreator implements JobCreator {
    @Override
    public Job create(String tag) {
        switch (tag) {
            case SyncTrackingCallsJob.TAG:
                return new SyncTrackingCallsJob();
            case SyncSurveyFormsJob.TAG:
                return new SyncSurveyFormsJob();
            default:
                return null;
        }
    }
}
