package hyenoon.com.sfts.job_schedule;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.Doctor;
import hyenoon.com.sfts.Entities.DoctorCheckInItem;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.ServiceModel.GsonFactory;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by syed.shah on 3/10/18.
 */

public class SyncSurveyFormsJob extends Job {
    public static final String TAG = "sync_forms_job_tag";
//    private static final int MAX_COUNT = 100;

    @NonNull
    @Override
    protected Result onRunJob(Params params) {
        return performSyncTask(getContext());
    }

    @Override
    protected void onReschedule(int newJobId) {
        // the rescheduled job has a new ID

    }

    public static void scheduleJob() {
        new JobRequest.Builder(SyncSurveyFormsJob.TAG)
                .setPersisted(true)
                .setRequiresDeviceIdle(false)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                .setUpdateCurrent(true)
                .setRequirementsEnforced(true)
                .build()
                .schedule();
    }

    private Result performSyncTask(Context context) {
        BasePreferenceHelper preferenceHelper = new BasePreferenceHelper(context);
        uploadSavedCheckedInDoctors(preferenceHelper);
        uploadAddedDoctors(preferenceHelper);
        uploadUpdatedDoctors(preferenceHelper);
        uploadSavedSurveyForm(preferenceHelper);
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(10));
        Log.d(TAG, "RESULT Return");
        return Result.SUCCESS;
    }

    public void uploadSavedCheckedInDoctors(final BasePreferenceHelper preferenceHelper) {
        final ArrayList<DoctorCheckInItem> checkInDoctors = preferenceHelper.getDoctorCheckInList();
        if (checkInDoctors != null
                && !checkInDoctors.isEmpty()) {

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String jsonData = gson.toJson(checkInDoctors);
            Log.d(TAG, jsonData);

            Call<WebResponse<Object>> service = WebServiceFactory.getInstance().checkInDoctors(
                    Constants.kWebCheckInDoctor, jsonData);
            service.enqueue(new Callback<WebResponse<Object>>() {
                @Override
                public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {
                        preferenceHelper.putDoctorCheckInList(new ArrayList<DoctorCheckInItem>());
                        broadcastMissedCalls(true, preferenceHelper);
                        Log.e(TAG, "uploadSavedCheckedInDoctors onResponse: SUCCESS");
                    } else {
                        Log.e(TAG, "uploadSavedCheckedInDoctors onResponse: FAILURE" + response.raw());
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                    Log.e(TAG, "uploadSavedCheckedInDoctors onFailure:" + t.getLocalizedMessage());
                    broadcastMissedCalls(false, preferenceHelper);
                }
            });
        } else Log.e(TAG, "uploadSavedCheckedInDoctors: NO DATA TO SYNC");
    }

    public void uploadAddedDoctors(final BasePreferenceHelper preferenceHelper) {
        ArrayList<Doctor> doctors = preferenceHelper.getAddedDoctorsList();
        if (doctors != null
                && !doctors.isEmpty()) {
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String jsonData = gson.toJson(doctors);
            Log.d(TAG, jsonData);

            Call<WebResponse<Object>> service = WebServiceFactory.getInstance().addDoctors(
                    Constants.kWebAddDoctor, jsonData);
            service.enqueue(new Callback<WebResponse<Object>>() {
                @Override
                public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                    if (response != null) {
                        if (response.body() != null
                                && response.body().getHeader().success.equals("1")) {
                            preferenceHelper.putAddDoctorList(new ArrayList<Doctor>());
                            Log.e(TAG, "uploadAddedDoctors onResponse: SUCCESS");
                        } else {
                            Log.e(TAG, "uploadAddedDoctors onResponse: FAILURE" + response.raw());
                        }
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                    Log.e(TAG, "uploadAddedDoctors onResponse: FAILURE" + t.getLocalizedMessage());
                }
            });
        } else Log.e(TAG, "uploadAddedDoctors: NO DATA TO SYNC");
    }

    public void uploadUpdatedDoctors(final BasePreferenceHelper preferenceHelper) {
        ArrayList<Doctor> doctors = preferenceHelper.getUpdatedDoctorsList();
        if (doctors != null
                && !doctors.isEmpty()) {
//                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            String jsonData = GsonFactory.getConfiguredGson().toJson(doctors);
            Log.d(TAG, jsonData);

            Call<WebResponse<Object>> service = WebServiceFactory.getInstance().updateDoctors(
                    Constants.kWebUpdateDoctor, jsonData);
            service.enqueue(new Callback<WebResponse<Object>>() {
                @Override
                public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                    if (response.body() != null
                            && response.body().getHeader().success.equals("1")) {
                        preferenceHelper.putUpdateDoctorList(new ArrayList<Doctor>());
                        Log.e(TAG, "uploadAddedDoctors onResponse: SUCCESS");
                    } else {
                        Log.e(TAG, "uploadAddedDoctors onResponse: FAILURE" + response.raw());
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                    Log.e(TAG, "uploadAddedDoctors onResponse: FAILURE" + t.getLocalizedMessage());
                }
            });
        } else Log.e(TAG, "uploadUpdatedDoctors: No Data To Sync");
    }

    private void uploadSavedSurveyForm(final BasePreferenceHelper preferenceHelper) {
        final JSONArray jsonArray = preferenceHelper.getSavedSurveryFormCheckInList();
        Log.e(TAG, "json array from sync service " + jsonArray);
        if (jsonArray != null && jsonArray.length() > 0) {
            Call<WebResponse<Object>> call = WebServiceFactory.getInstance().checkin(Constants.CHECK_IN, jsonArray);
            call.enqueue(new Callback<WebResponse<Object>>() {
                @Override
                public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                    if (response.body() != null && response.body().isSuccess()) {
                        preferenceHelper.removeCheckInFormPreference();
                        broadcastMissedCalls(true, preferenceHelper);
                        Log.e(TAG, "uploadAddedDoctors onResponse: SUCCESS");
                    } else {
                        Log.e(TAG, "uploadAddedDoctors onResponse: FAILURE" + response.raw());
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                    Log.e(TAG, "uploadAddedDoctors onResponse: FAILURE" + t.getLocalizedMessage());
                    broadcastMissedCalls(false, preferenceHelper);
                }
            });
        } else Log.e(TAG, "*** uploadSavedSurveyForm *** JsonArray is Null");
    }

    private void broadcastMissedCalls(boolean isSuccess, BasePreferenceHelper prefHelper) {
        if (isSuccess) {
            prefHelper.putMissedCheckinCalls(0);
        } else {
            int noOfMissedCheckinCalls = prefHelper.getMissedCheckinCalls();
            if (noOfMissedCheckinCalls == -1)
                noOfMissedCheckinCalls = 0;
            prefHelper.putMissedCheckinCalls(++noOfMissedCheckinCalls);
        }

        Intent intent = new Intent(Constants.kUIACTION);
        intent.putExtra(Constants.kACTION, "checkinCalls");
        intent.putExtra(Constants.kNoOfCheckinCalls, prefHelper.getMissedCheckinCalls());
        Utils.triggerBroadcast(getContext(), intent);
    }
}
