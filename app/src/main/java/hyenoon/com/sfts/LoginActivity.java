package hyenoon.com.sfts;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.UserWrapper;
import hyenoon.com.sfts.Entities.Users;
import hyenoon.com.sfts.HelperClasses.LoginPreference;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

@RuntimePermissions
public class LoginActivity extends BaseActivity {

//    private SweetAlertDialog pDialog;

    private EditText edtTxtUserName;
    private EditText edtTxtPassword;
    private String TAG = getClass().getName();
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        edtTxtUserName = (EditText) findViewById(R.id.edtTxtUserName);
        edtTxtPassword = (EditText) findViewById(R.id.edtTxtPassword);

    }

    @NeedsPermission({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void setupContactAccess() {

    }

    @OnShowRationale({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void showStorageAccessRationale(final PermissionRequest request) {
        openSettinPage();
        request.proceed();
    }

    @OnPermissionDenied({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void storageAccessDenied() {
        Toast.makeText(this, "Access Denied, Please grant access to complete process", Toast.LENGTH_LONG);
    }

    @OnNeverAskAgain({CALL_PHONE, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION})
    void openSetting() {
        openSettinPage();
    }


    private void openSettinPage() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        LoginActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LoginActivityPermissionsDispatcher.setupContactAccessWithPermissionCheck(LoginActivity.this);

    }

    public Context getContext() {
        return (Context) this;
    }

    public void onClickLogin(View v) {
        Log.d("TAG", "Login button clicked");

        if (validateFields()) {

            // Call login webservice
            pDialog = Utils.showSweetLoader(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Loading");

            Call<WebResponse<UserWrapper>> loginService = WebServiceFactory.getInstance().loginUser(
                    Constants.kWebLoginUser,
                    edtTxtUserName.getText().toString(),
                    edtTxtPassword.getText().toString());

            loginService.enqueue(new Callback<WebResponse<UserWrapper>>() {
                @Override
                public void onResponse(Call<WebResponse<UserWrapper>> call, Response<WebResponse<UserWrapper>> response) {
                    Utils.hideSweetLoader(pDialog);

                    if (response != null) {
                        if (response.body() != null
                                && response.body().getHeader().success.equals("1")) {

                            Users objUser = response.body().getBody().getUsers();
                            if (objUser != null) {
                                LoginPreference.setLoginStatus(context, true);
                                prefHelper.putUser(objUser);
                                startHomeActivity();
                            }
                        } else {
                            if (response.body() != null)
                                Utils.showShortToastInCenter(getContext(), response.body().getHeader().message);
                            else {
                                Utils.showShortToastInCenter(getContext(), "Service failed. Please check your internet");
                            }
                        }
                    } else {
                        Log.e(TAG, "Found Null in web Response,");
                        Utils.showShortToastInCenter(getContext(), "Service failed. Please check your internet");
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<UserWrapper>> call, Throwable t) {
                    Utils.hideSweetLoader(pDialog);
                    Utils.showShortToastInCenter(getContext(), "Something went wrong. Please try again");
                }
            });
        }
    }

    private void startHomeActivity() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean validateFields() {
        EditText[] allFields = {edtTxtUserName, edtTxtPassword};
        for (EditText field : allFields) {
            String text = field.getText().toString();
            if (TextUtils.isEmpty(text)) {
                return false;
            }
        }

        return true;
    }
}


/*
package hyenoon.com.sfts;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.UserWrapper;
import hyenoon.com.sfts.Entities.Users;
import hyenoon.com.sfts.HelperClasses.LoginPreference;
import hyenoon.com.sfts.HelperClasses.PreferenceHelper;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends BaseActivity {

//    private SweetAlertDialog pDialog;

    private EditText edtTxtUserName;
    private EditText edtTxtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtTxtUserName = (EditText) findViewById(R.id.edtTxtUserName);
        edtTxtPassword = (EditText) findViewById(R.id.edtTxtPassword);
    }

    public Context getContext() {
        return (Context)this;
    }

    public void onClickLogin(View v) {
        Log.d("TAG", "Login button clicked");

        if (validateFields()) {

            // Call login webservice
            pDialog = Utils.showSweetLoader(this, SweetAlertDialog.PROGRESS_TYPE, "Loading");

            Call<WebResponse<UserWrapper>> loginService = WebServiceFactory.getInstance().loginUser(
                    Constants.kWebLoginUser,
                    edtTxtUserName.getText().toString(),
                    edtTxtPassword.getText().toString());

            loginService.enqueue(new Callback<WebResponse<UserWrapper>>() {
                @Override
                public void onResponse(Call<WebResponse<UserWrapper>> call, Response<WebResponse<UserWrapper>> response) {
                    Utils.hideSweetLoader(pDialog);

                    if (response != null) {
                        if (response.body() != null
                                && response.body().getHeader().success.equals("1")) {

                            Users objUser = response.body().getBody().getUsers();
                            if (objUser != null) {
                                prefHelper.putUser(objUser);
                                LoginPreference.setLoginStatus(LoginActivity.this,true);
                                startHomeActivity();
                            }
                        }
                        else {
                            if (response.body() != null)
                                Utils.showShortToastInCenter(getContext(), response.body().getHeader().message);
                            else {
                                Utils.showShortToastInCenter(getContext(), "Service failed. Please check your internet");
                            }
                        }
                    }
                    else {
                        Utils.showShortToastInCenter(getContext(), "Service failed. Please check your internet");
                    }
                }
                @Override
                public void onFailure(Call<WebResponse<UserWrapper>> call, Throwable t) {
                    Utils.hideSweetLoader(pDialog);
                    Utils.showShortToastInCenter(getContext(), "Something went wrong. Please try again");
                }
            });
        }
    }

    private void startHomeActivity() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean validateFields() {
        EditText[] allFields = { edtTxtUserName, edtTxtPassword };
        for (EditText field: allFields) {
            String text = field.getText().toString();
            if (TextUtils.isEmpty(text)) {
                return false;
            }
        }

        return true;
    }
}
*/
