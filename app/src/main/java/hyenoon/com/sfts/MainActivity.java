package hyenoon.com.sfts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.splunk.mint.Mint;

import hyenoon.com.sfts.HelperClasses.LoginPreference;

public class MainActivity extends BaseActivity {

    public static final int SPLASH_TIMER = 3 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Mint.initAndStartSession(MainActivity.this, "2828d17d");
//        Intent intent = new Intent(mContext, MyInstanceIDListenerService.class);
//        startService(intent);
        startSplashTimer();
    }

    private void startSplashTimer() {
        Thread timerSplash = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(SPLASH_TIMER);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } finally {

                    if (LoginPreference.getLoginStatus(MainActivity.this)) {
                        startHomeActivity();
                    }
                    else {
                        startLoginActivity();
                    }
                }
            }
        };
        timerSplash.start();
    }

    private void startLoginActivity() {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void startHomeActivity() {
        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
