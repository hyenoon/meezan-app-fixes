package hyenoon.com.sfts;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.CheckInModule;
import hyenoon.com.sfts.Entities.ProductLOV;
import hyenoon.com.sfts.Entities.ProductModel;
import hyenoon.com.sfts.Fragments.Dialogfragment;
import hyenoon.com.sfts.ServiceModel.GsonFactory;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckInFormActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, Dialogfragment.clickListener, Dialogfragment.sumitClick {
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> status = new ArrayList<>();
    TextView tv_LabelSpinner, tv_LabelStatus, tv_Date, tv_PhoneNumner;
    ArrayList<String> seletedItems = new ArrayList();
    boolean[] chechkItems;
    int selectedPostion;
    AlertDialog.Builder dialog;
    private DatePickerDialog dpd;
    int listsize;
    private int year, month, dayOfMonth;
    private Calendar now = Calendar.getInstance();
    List<ProductLOV> lovList = new ArrayList<>();
    DatePickerDialog datePickerDialog;
    private String number;
    private String TAG = getClass().getName();
    CharSequence[] items = {"Not interested", "Need follow up"};
    private EditText et_TotalAmount;
    ArrayList<CheckInModule> checkInModuleArrayList = new ArrayList<>();
    ArrayList<String> checkInlist;
    ProductModel productModel;
    Dialogfragment dFragment;
    ProductLOV valueOf;
    private String date;
    FragmentManager fm = getSupportFragmentManager();
    private int selctedPostions = -1;
    private boolean[] selctedPostionsarray={};
    private int count = 0;
    private String type;
    private String customerName;
    private String companyName;
    private String address;
    private String emailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_check_in_form);

        if (getIntent().getStringExtra("number") != null)
            number = getIntent().getStringExtra("number");
        type = getIntent().getStringExtra("type");

        customerName = getIntent().getStringExtra("name");
        companyName = getIntent().getStringExtra("companyName");
        address = getIntent().getStringExtra("address");
        emailAddress = getIntent().getStringExtra("emailAddress");


        arrayList.add("Select Item");
        arrayList.add("Card");
        arrayList.add("Visa Card");
        arrayList.add("Master Card");
        arrayList.add("Car Loan");
        status.add("Active");
        status.add("Inactive");
        status.add("Follow Up");

        init();

        fetchLOV();

        chechkItems = new boolean[arrayList.size()];

        chechkItems[0] = false;
    }

    @Override
    public void onBackPressed() {

    }

    /*public CheckInFormActivity(String callNumber)
    {
        this.number = callNumber;
    }*/

    private void init() {
        valueOf = new ProductLOV();
        et_TotalAmount = (EditText) findViewById(R.id.edt_serveform_amout);
        tv_LabelSpinner = (TextView) findViewById(R.id.tv_items);
        tv_LabelStatus = (TextView) findViewById(R.id.tv_selectStatus);
        tv_Date = (TextView) findViewById(R.id.edt_serveform_date);
        tv_PhoneNumner = (TextView) findViewById(R.id.tv_phonenumber);
        tv_PhoneNumner.setText(number);
        tv_Date.setOnClickListener(this);
    }

    public void onClickItemSelected(View view) {
        Log.e(TAG, "Size " + lovList.size());
        dFragment = new Dialogfragment();

        Bundle bundle = new Bundle();
        bundle.putString("json", getJsonDoctorsList(lovList));
        bundle.putBooleanArray("selectedPosition", selctedPostionsarray);

        dFragment.setArguments(bundle);
        dFragment.show(fm, "Dialog Fragment");
        dFragment.setSumitClick(this);

       /* productModel = new ProductModel();
        final CharSequence[] items = {" Car Loan ", " Visa Debit Card ", " Car Ijarah ", " Term Deposit LCY "};


        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMultiChoiceItems(items, chechkItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            selectedPostion = indexSelected;
                            //productModel.setProductIndex(indexSelected);
                            productModel.setProductIndex(indexSelected);
                            seletedItems.add(productModel.getProductIndex());



                            //To be fixed(Products ID)
                            productModel.setProductID(String.valueOf(items[indexSelected]));
                            seletedItems.add(productModel.getProductID());

                            chechkItems[indexSelected] = true;
                        } else if (seletedItems.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            seletedItems.remove(Integer.valueOf(indexSelected));
                            seletedItems.remove(indexSelected);
                            chechkItems[indexSelected] = false;
                        }
                    }
                }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (seletedItems.size() > 1)
                            tv_LabelSpinner.setText(seletedItems.size() + " Items Selected");
                        else if (seletedItems.size() == 1) {
                            for (int i = 0; i < chechkItems.length; i++) {
                                if (chechkItems[i])
                                    tv_LabelSpinner.setText(items[i]);
                            }
                        } else
                            tv_LabelSpinner.setText("Select Items");

                    }
                }).create();
        dialog.setCancelable(false);
        dialog.show();*/
    }

    public void onClickStatusItemSelected(View view) {
        dialog = new AlertDialog.Builder(CheckInFormActivity.this);
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tv_LabelStatus.setText(items[which]);
                selectedPostion = which;
                if (which == 1) {

                    year = now.get(Calendar.YEAR);
                    month = now.get(Calendar.MONTH);
                    dayOfMonth = now.get(Calendar.DAY_OF_MONTH);
                    datePickerDialog = new DatePickerDialog(
                            CheckInFormActivity.this, CheckInFormActivity.this, year, month, dayOfMonth);
                    datePickerDialog.show();

                    //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 10000);

                    // Calender Crash fixed..
                    datePickerDialog.getDatePicker().setMinDate(new Date().getTime() - 10000);

                    tv_Date.setVisibility(View.VISIBLE);

                } else {
                    if (tv_Date.getVisibility() == View.VISIBLE) {
                        tv_Date.setVisibility(View.GONE);
                    }
                }

            }
        });

        dialog.create();
        dialog.show();


    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        tv_Date.setText("Date : " + year + "/" + monthOfYear + "/" + dayOfMonth);
        date = year + "/" + monthOfYear + "/" + dayOfMonth;
    }

    @Override
    public void onClick(View v) {
        datePickerDialog.show();
    }

    public void onClickSubmit(View view) throws JSONException {
        Set<String> currentLocation = prefHelper.getCurrentLocation(mContext);
        if (currentLocation == null) {
            Utils.showLongToastInCenter(mContext, "Unable to get your current location. Please check your GPS Settings.");
            return;
        }
        List<String> locations = new ArrayList<>(currentLocation);
        Log.e("Nearby api hit ", "latitude " + locations.get(0) + " longitude " + locations.get(1));

        String[] productsID = new String[seletedItems.size()];

        for (int i = 0; i < seletedItems.size(); i++) {
            productsID[i] = String.valueOf(seletedItems.get(i));
            Log.e(TAG, "Product ID " + productsID[i]);

        }
        // to be fixed according to Cusomters status list
        //  String curstomersStatus = (String) items[selectedPostion];

        pDialog = Utils.showSweetLoader(CheckInFormActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Submiting...");
        CheckInModule checkInModule = new CheckInModule(prefHelper.getUser().getUserId(), number, locations.get(1), locations.get(0),
                et_TotalAmount.getText().toString(), selectedPostion, type, date, String.valueOf(System.currentTimeMillis()), productsID,
                customerName, companyName, emailAddress, address
        );

        checkInModuleArrayList.add(checkInModule);

        for (int i = 0; i < seletedItems.size(); i++) {
            Log.e(TAG, "Selected Items " + seletedItems.get(i));
        }

//        Log.e(TAG, "josn array checkinactivity " + makeJSON().toString());

        if (!Utils.isNetworkConnected(this)) {
            prefHelper.putSavedSurveyFormCheckInList(makeJSON());
            Utils.hideSweetLoader(pDialog);
            Toast.makeText(CheckInFormActivity.this, "Submitted Successfully", Toast.LENGTH_SHORT).show();
            Intent homeActivity = new Intent(CheckInFormActivity.this, HomeActivity.class);
            homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeActivity);

        } else {

            Call<WebResponse<Object>> call = WebServiceFactory.getInstance().checkin(
                    Constants.CHECK_IN,
                    makeJSON());


            call.enqueue(new Callback<WebResponse<Object>>() {
                @Override
                public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                    Utils.hideSweetLoader(pDialog);
                    if (response.body() == null)
                        Toast.makeText(CheckInFormActivity.this, Constants.kNullRespone, Toast.LENGTH_SHORT).show();
                    else if (response.body().isSuccess()) {
                        Toast.makeText(CheckInFormActivity.this, "Sumitted Successfully", Toast.LENGTH_SHORT).show();

                        Log.e(TAG, "onSuccess " + response.body().getMessage());

                        Intent homeActivity = new Intent(CheckInFormActivity.this, HomeActivity.class);
                        homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeActivity);

                    } else {
                        Toast.makeText(CheckInFormActivity.this, Constants.kNullRespone, Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onError " + response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                    Utils.hideSweetLoader(pDialog);
                    Toast.makeText(CheckInFormActivity.this, Constants.kNullRespone, Toast.LENGTH_SHORT).show();
                    Log.e(TAG, t.getMessage());
                }
            });
        }
    }

    private JSONArray makeJSON() throws JSONException {
        checkInlist = new ArrayList<>();


        JSONArray jsonProductArray = new JSONArray();
        for (int i = 0; i < selctedPostionsarray.length; i++) {
            if (selctedPostionsarray[i])
                jsonProductArray.put(lovList.get(i).getProductId());
        }

        JSONObject jsonObject = new JSONObject();


        Log.e(TAG, "lsit size " + checkInModuleArrayList.size());
        for (int i = 0; i < checkInModuleArrayList.size(); i++) {
            Log.e(TAG, "Customer Status " + checkInModuleArrayList.get(i).getCustomer_status());
            jsonObject.put("userid", checkInModuleArrayList.get(i).getUserID());
            jsonObject.put("customer_phone_number", checkInModuleArrayList.get(i).getCustomerPhoneNumber());
            jsonObject.put("latitude", checkInModuleArrayList.get(i).getLat());
            jsonObject.put("longitude", checkInModuleArrayList.get(i).getLng());
            jsonObject.put("product_id", jsonProductArray);
            jsonObject.put("total_amount", checkInModuleArrayList.get(i).getTotal_amount());
            jsonObject.put("customer_status", checkInModuleArrayList.get(i).getCustomer_status());
            jsonObject.put("visit_type", checkInModuleArrayList.get(i).getVisit_type());
            if (tv_Date.getText().toString().isEmpty())
                jsonObject.put("followup_date", "");
            else
                jsonObject.put("followup_date", checkInModuleArrayList.get(i).getFollowup_date());
            jsonObject.put("visited_time", System.currentTimeMillis());
            jsonObject.put("customer_name", checkInModuleArrayList.get(i).getCustomerName());
            jsonObject.put("company_name", checkInModuleArrayList.get(i).getCompanyName());
            jsonObject.put("email_address", checkInModuleArrayList.get(i).getEmailAddress());
            jsonObject.put("address", checkInModuleArrayList.get(i).getAddress());


        }

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);


        return jsonArray;
    }

    public void fetchLOV() {


        lovList = prefHelper.getLOV();
        //Log.e(TAG,"Size "+lovList.size());
        if (lovList != null && lovList.size() > 0)
            selctedPostionsarray = new boolean[lovList.size()];
       /* pDialog = Utils.showSweetLoader(CheckInFormActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Fetching LOV");
        Log.e(TAG, "fetchLOV Called");

        Call<WebResponse<BodyLOV>> call = WebServiceFactory.getInstance().getLOV(Constants.GET_LOV);
        call.enqueue(new Callback<WebResponse<BodyLOV>>() {
            @Override
            public void onResponse(Call<WebResponse<BodyLOV>> call, Response<WebResponse<BodyLOV>> response) {
                Utils.hideSweetLoader(pDialog);
                if (response.body() == null)
                    Toast.makeText(CheckInFormActivity.this, "something went wrong,please check your internet connectivity", Toast.LENGTH_SHORT).show();
                else if (response.body().isSuccess()) {

                    lovList = response.body().getBody().getLov().getProducts();
                    selctedPostionsarray = new boolean[lovList.size()];
                    Log.e(TAG, "Size " + lovList.size());
                } else
                    Toast.makeText(CheckInFormActivity.this, "something went wrong,please check your internet connectivity", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<WebResponse<BodyLOV>> call, Throwable t) {
                Utils.hideSweetLoader(pDialog);

                Log.e(TAG, "onFailure " + t.getMessage());
                Toast.makeText(CheckInFormActivity.this, "something went wrong,please check your internet connectivity", Toast.LENGTH_SHORT).show();

            }
        });*/

    }

    private String getJsonDoctorsList(List<ProductLOV> data) {
        return GsonFactory.getConfiguredGson().toJson(data);
    }

    @Override
    public void postion(boolean isChecked, int postion) {

        selctedPostions = postion;
        if (isChecked) {
            count++;

            Log.e(TAG, "Count true " + count);
            selctedPostionsarray[postion] = true;
            /*valueOf.setProductId(lovList.get(postion).getProductId());
            seletedItems.add(valueOf.getProductId());
            // If the user checked the item, add it to the selected items
            selectedPostion = postion;
            Log.e(TAG, "Count is " + count);
            chechkItems[postion] = true;*/
        } else {
            count--;
            Log.e(TAG, "Count false" + count);
            selctedPostionsarray[postion] = false;
            /*count--;
            Log.e(TAG, "Count is " + count);
            // Else, if the item is already in the array, remove it
            seletedItems.remove(postion);
          //  seletedItems.remove(postion);
            chechkItems[postion] = false;*/
        }

    }

    @Override
    public void submit() {

        for (int i = 0; i < selctedPostionsarray.length; i++) {

            if (selctedPostionsarray[i]) {
                seletedItems.add(lovList.get(i).getProductId());
            }
        }

        Log.e(TAG, "Count " + count);

        if (count > 1)
            tv_LabelSpinner.setText(count + " Items Selected");
        else if (count == 1) {
            for (int i = 0; i < selctedPostionsarray.length; i++) {
                if (selctedPostionsarray[i])
                    tv_LabelSpinner.setText(lovList.get(i).getProductName());
            }
        } else
            tv_LabelSpinner.setText("Select Items");

    }

}
