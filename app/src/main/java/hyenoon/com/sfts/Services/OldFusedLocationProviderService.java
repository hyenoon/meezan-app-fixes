package hyenoon.com.sfts.Services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.UserLocation;
import hyenoon.com.sfts.Entities.Users;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.ServiceModel.GsonFactory;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import hyenoon.com.sfts.job_schedule.SyncTrackingCallsJob;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OldFusedLocationProviderService extends Service implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "FusedLocationService";
    private static long INTERVAL = 1000 * 60 * 1;
    private static long FASTEST_INTERVAL = 1000 * 60 * 1;

//    private static Timer timer = new Timer();
//    DBHelper dbHelper;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation;
    BasePreferenceHelper prefHelper;

    public OldFusedLocationProviderService() {
//        this.locationUpdateListener = null;
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            Log.e(TAG, "Location update started ..............: ");
        } catch (SecurityException ex) {
            // Eat exception.
        }
    }

    protected boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            return false;
        }
    }

    protected void startUpdateLocationTimer() {
        SyncTrackingCallsJob.scheduleJob();
//        long interval = 2 * 60 * 1000; // 2 minutes
//        timer.scheduleAtFixedRate(new updateLocationTask(), 0, interval);
    }

    public void addUserLocationToList() {
        Log.e(TAG, "addUserLocationToList is called");
        if (prefHelper.getUser() != null) {
            Log.e(TAG, "user avaiable");
            String currentDateTime = DateFormat.getDateTimeInstance().format(new Date());
            String userId = prefHelper.getUser().getUserId();

            UserLocation location = new UserLocation();
            location.setUserId(userId);
            location.setLat(String.valueOf(mCurrentLocation.getLatitude()));
            location.setLng(String.valueOf(mCurrentLocation.getLongitude()));
            location.setLocationTime(mCurrentLocation.getTime());
            location.setBatteryLevel(Utils.getBattreryLevel(this));
            String version = Utils.getAppVersion(getApplicationContext());
            location.setVersionNo((version != null) ? version : "0"); // if failed to get version set 0
            ArrayList<UserLocation> locations = prefHelper.getUserLocationList();
            if (locations == null)
                locations = new ArrayList<UserLocation>();

            locations.add(location);
            prefHelper.putUserLocationList(locations);
//            dbHelper.insertLcoation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            Users users = prefHelper.getUser();
            users.setmCurrentLocation(mCurrentLocation);
            prefHelper.putUser(users);
        } else {
            Log.e(TAG, "User not found");
        }
    }

    private void startAlarmManager() {
        Log.e(TAG, "startAlarmManager");
        Intent myIntent = new Intent(getApplicationContext(), OldFusedLocationProviderService.class);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 10);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

//        Toast.makeText(getApplicationContext(), "Start Alarm", Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "OldFusedLocationProviderService onCreate");
//        dbHelper = new DBHelper(getApplication());

        prefHelper = new BasePreferenceHelper(getApplicationContext());

        //show error dialog if GooglePlayServices not available
        if (isGooglePlayServicesAvailable()) {
            createLocationRequest();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            startUpdateLocationTimer();
        } else {
            Toast.makeText(getApplicationContext(), "Google play service not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "Location service starts", Toast.LENGTH_SHORT).show();

        if (mGoogleApiClient == null)
            return START_NOT_STICKY;
        mGoogleApiClient.connect();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();

        Log.e(TAG, "onDestroy OldFusedLocationProviderService");
        if (prefHelper.getShiftStatus())
            startAlarmManager();
        super.onDestroy();

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        mGoogleApiClient.disconnect();

        Log.e(TAG, "onTaskRemoved OldFusedLocationProviderService");
        if (prefHelper.getShiftStatus())
            startAlarmManager();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e(TAG, "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed: " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.e(TAG, "onLocationChanged:[" + location.getLatitude() + "," + location.getLongitude() + "]");
        Intent intent = new Intent(Constants.kUIACTION);
        intent.putExtra(Constants.kACTION, "location");

        if (location.getLatitude() < 1 & location.getLongitude() < 1) {
            Log.e(TAG, "location is 0,0");

            intent.putExtra(Constants.kLAT, "0");
            intent.putExtra(Constants.kLONG, "0");
            Log.e(TAG, prefHelper.getNoOfZeroLocationFound() + "");
            if (checkLocationZero()) {
                Log.e(TAG, " true " + prefHelper.getNoOfZeroLocationFound() + "");
                prefHelper.putNoOfZeroLocationFound(0);
                intent.putExtra(Constants.kLocationZero, 0);

            } else {

                int num = prefHelper.getNoOfZeroLocationFound();
                prefHelper.putNoOfZeroLocationFound(++num);
                Log.e(TAG, "false " + prefHelper.getNoOfZeroLocationFound() + "");
            }
//            Toast.makeText(getApplicationContext(), "Location is 0,0 ", Toast.LENGTH_SHORT).show();
//            LocalBroadcastManager
//                    .getInstance(OldFusedLocationProviderService.this)
//                    .sendBroadcast(new Intent(Constants.kUIACTION)
//                            .putExtra("location", true)
//                            .putExtra("action", "location"));

            Utils.triggerBroadcast(OldFusedLocationProviderService.this, intent);

            return;
        }


        if (Utils.isBetterLocation(location, mCurrentLocation)) {
            Log.e(TAG, "better location ");

            intent.putExtra(Constants.kLocationZero, 1);
            mCurrentLocation = location;
            addUserLocationToList();
            intent.putExtra(Constants.kLAT, location.getLatitude() + "");
            intent.putExtra(Constants.kLONG, location.getLongitude() + "");
            Utils.triggerBroadcast(OldFusedLocationProviderService.this, intent);
            prefHelper.putCurrentLocation(OldFusedLocationProviderService.this, location.getLatitude(), location.getLongitude());
        } else {
            intent.putExtra(Constants.kLocationZero, -1);
            Log.e(TAG, "Failed to pass better location");
        }
    }

    public boolean checkLocationZero() {


        int num = prefHelper.getNoOfZeroLocationFound();
        if (num == -1)
            num = 0;


        return (num >= 3);
    }

    private class updateLocationTask extends TimerTask {
        public void run() {
            Log.e(TAG, "Update location task executed..");
//            Log.e(TAG, " battery level " + Utils.getBattreryLevel(OldFusedLocationProviderService.this));
            updateUserLocation();
        }

        public void updateUserLocation() {
            Log.e(TAG, "Webservice called: " + Constants.kWebUpdateLocation);

            ArrayList<UserLocation> locations = prefHelper.getUserLocationList();
//            ArrayList<String> userLocationlist = dbHelper.getLocations();

            if (locations != null && !locations.isEmpty()) {

                final ArrayList<UserLocation> listUserLocations = new ArrayList<>();
                int totalCount = (locations.size() < 100) ? locations.size() : 100;
                for (int index = 0; index < totalCount; index++) {
                    listUserLocations.add(locations.get(index));
                }
                String jsonData = GsonFactory.getConfiguredGson().toJson(listUserLocations);
                Log.e(TAG, jsonData);

                Call<WebResponse<Object>> service = WebServiceFactory.getInstance().updateUserLocation(
                        Constants.kWebUpdateLocation, jsonData);
                service.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        if (response != null) {
                            if (response.body() != null
                                    && response.body().getHeader().success.equals("1")) {
                                Intent netIntent = new Intent(Constants.kUIACTION);
                                netIntent.putExtra(Constants.kACTION, "status");
                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
                                Utils.triggerBroadcast(OldFusedLocationProviderService.this, netIntent);

                                prefHelper.putMissedSynCalls(0);
                                Intent intent = new Intent(Constants.kUIACTION);
                                intent.putExtra(Constants.kACTION, "trackingCalls");
                                intent.putExtra(Constants.kNoOfTrackingCalls, prefHelper.getMissedSynCalls());
                                Utils.triggerBroadcast(OldFusedLocationProviderService.this, intent);
//                                LocalBroadcastManager.getInstance(OldFusedLocationProviderService.this)
//                                        .sendBroadcast(new Intent(Constants.kUIACTION)
//                                                .putExtra("missedCalls", prefHelper.getMissedSynCalls() + "")
//                                                .putExtra("action", "missedCalls"));
                                Log.e(TAG, "Successfully updated user locations: " + listUserLocations.size());
                                removeUserLocations();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        Intent netIntent = new Intent(Constants.kUIACTION);
                        netIntent.putExtra(Constants.kACTION, "status");
                        Log.e(TAG, "Webservice failed: " + t.getLocalizedMessage());
                        if (Utils.isNetworkConnected(OldFusedLocationProviderService.this)) {

                            if (Utils.isInternetAvailable()) {
                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
                            }
                        } else {
                            netIntent.putExtra(Constants.kInternetStatus, "Not Connected");

                        }
                        Utils.triggerBroadcast(OldFusedLocationProviderService.this, netIntent);
                        int noOfMissedCalls = prefHelper.getMissedSynCalls();
                        if (noOfMissedCalls == -1)
                            noOfMissedCalls = 0;
                        prefHelper.putMissedSynCalls(++noOfMissedCalls);
                        Log.e(TAG, " missed calls " + prefHelper.getMissedSynCalls());
                        //Toast.makeText(OldFusedLocationProviderService.this, "Missed Calls " + prefHelper.getMissedSynCalls(), Toast.LENGTH_SHORT).show();
                        //                       LocalBroadcastManager.getInstance(OldFusedLocationProviderService.this).sendBroadcast(new Intent(Constants.kUIACTION).putExtra("missedCalls", prefHelper.getMissedSynCalls() + "").putExtra("action", "missedCalls"));
                        Intent intent = new Intent(Constants.kUIACTION);
                        intent.putExtra(Constants.kACTION, "trackingCalls");
                        intent.putExtra(Constants.kNoOfTrackingCalls, prefHelper.getMissedSynCalls());
                        Utils.triggerBroadcast(OldFusedLocationProviderService.this, intent);

                    }
                });
            } else {

                //Toast.makeText(OldFusedLocationProviderService.this, "location list in pref is empty", Toast.LENGTH_SHORT).show();

                Log.e(TAG, "location list in pref is empty");
            }
        }

        public void removeUserLocations() {

//            boolean isClear = dbHelper.clearAll();
//            if (isClear) {
//                Log.e(TAG, "Location Clear");
//
//            }

/*            ArrayList<UserLocation> locations = prefHelper.getUserLocationList();
            if (locations != null
                    && !locations.isEmpty()) {
                if (listUserLocations != null) {

                    for (int x = 0; x < listUserLocations.size(); x++) {
                        UserLocation userLocation = listUserLocations.get(x);
                        for (int y = 0; y < locations.size(); y++) {
                            UserLocation location = locations.get(y);
                            if (userLocation.getLocationTime() == location.getLocationTime()) {
                                locations.remove(y);
                                break;
                            }
                        }
                    }

                    prefHelper.putUserLocationList(locations);
                }
            }*/
        }

    }
}