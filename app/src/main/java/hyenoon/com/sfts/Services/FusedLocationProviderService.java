package hyenoon.com.sfts.Services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Calendar;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.UserLocation;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.Utils.ServiceUtil;
import hyenoon.com.sfts.Utils.Utils;
import hyenoon.com.sfts.job_schedule.SyncTrackingCallsJob;

/**
 * Created by Arsalan on 5/7/2017.
 */

public class FusedLocationProviderService extends Service implements LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "FusedLocationService";

    // changed for better accuracy
    private static long INTERVAL = 60 * 1000;
    private static long FASTEST_INTERVAL = 60 * 1000;

    BasePreferenceHelper prefHelper;
    LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;

    public FusedLocationProviderService() {

    }

    @Override
    public void onCreate() {
        Log.e(TAG, "FusedLocationProviderService onCreate");
        prefHelper = new BasePreferenceHelper(getApplicationContext());
        //show error dialog if GooglePlayServices not available
        if (isGooglePlayServicesAvailable()) {
            createLocationRequest();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addApi(ActivityRecognition.API)
                    .addOnConnectionFailedListener(this)
                    .build();
            startUpdateLocationTimer();
        } else {
            Toast.makeText(getApplicationContext(), "Google play service not available", Toast.LENGTH_LONG).show();
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            return false;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "Location service starts", Toast.LENGTH_SHORT).show();
        mGoogleApiClient.connect();
        return Service.START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        Log.e(TAG, "onTaskRemoved FusedLocationProviderService");
        if (prefHelper.getShiftStatus()) {
            startAlarmManager();
        }

        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        Log.e(TAG, "onDestroy FusedLocationProviderService");
        if (prefHelper.getShiftStatus())
            startAlarmManager();
        super.onDestroy();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected ...............: " + mGoogleApiClient.isConnected());
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "Connection Suspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Connection failed: " + connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        int kLocation = -1;
        Log.e(TAG, "onLocationChanged:[" + location.getLatitude() + "," + location.getLongitude() + "]");
        if (location.getLatitude() < 1){
            if (checkLocationZero()){
                prefHelper.putNoOfZeroLocationFound(0);
                kLocation = 0;
            }else {
                int num = prefHelper.getNoOfZeroLocationFound();
                prefHelper.putNoOfZeroLocationFound(++num);
            }
            broadcastTracking(location, kLocation);
            return;
        }
        if (ServiceUtil.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location;
            addUserLocationToList();
            prefHelper.putCurrentLocation(this, location.getLatitude(), location.getLongitude());
            kLocation = 1;
        } else {
            Log.e(TAG, "Failed to pass better location");
        }
        broadcastTracking(location, kLocation);
    }

    private void broadcastTracking(Location location, int kLocation) {
        Intent intent = new Intent(Constants.kUIACTION);
        intent.putExtra(Constants.kACTION, "location");
        intent.putExtra(Constants.kLAT, location.getLatitude() + "");
        intent.putExtra(Constants.kLONG, location.getLongitude() + "");
        intent.putExtra(Constants.kLocationZero, kLocation);
        Utils.triggerBroadcast(this, intent);
    }

    public boolean checkLocationZero() {
        int num = prefHelper.getNoOfZeroLocationFound();
        if (num == -1)
            num = 0;
        return (num >= 3);
    }

    private void startAlarmManager() {
        Log.e(TAG, "startAlarmManager");
        Intent myIntent = new Intent(this, FusedLocationProviderService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, myIntent,
                PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 10);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    protected void startLocationUpdates() {
        try {
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            Log.e(TAG, "Location update started");
        } catch (SecurityException ex) {
            // Eat exception.
        }
    }

    protected void startUpdateLocationTimer() {
        SyncTrackingCallsJob.scheduleJob();
//        timer.scheduleAtFixedRate(new updateLocationTask(), 0, SYNC_DATA_TIMER);
    }

    public void addUserLocationToList() {
        if (prefHelper.getUser() != null) {
            Log.e(TAG, "user available");
            String userId = prefHelper.getUser().getUserId();
            UserLocation location = new UserLocation();
            location.setUserId(userId);
            location.setLat(String.valueOf(mCurrentLocation.getLatitude()));
            location.setLng(String.valueOf(mCurrentLocation.getLongitude()));
            location.setLocationTime(mCurrentLocation.getTime());
            location.setBatteryLevel(Utils.getBattreryLevel(this));
            String version = Utils.getAppVersion(getApplicationContext());
            location.setVersionNo((version != null) ? version : "0"); // if failed to get version set 0
            ArrayList<UserLocation> locations = prefHelper.getUserLocationList();
            if (locations == null)
                locations = new ArrayList<>();
            locations.add(location);
            prefHelper.putUserLocationList(locations);
            prefHelper.putCurrentLocation(FusedLocationProviderService.this, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        } else {
            Log.e(TAG, "User not found");
        }
    }
}