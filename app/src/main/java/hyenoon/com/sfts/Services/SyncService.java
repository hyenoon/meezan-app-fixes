package hyenoon.com.sfts.Services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

import hyenoon.com.sfts.job_schedule.SyncSurveyFormsJob;

/**
 * Created by dev.akber on 7/25/2016.
 */

public class SyncService extends Service {

    private static final String TAG = "SyncService";

//    private static long INTERVAL = 1000 * 60 * 2;
//    private static Timer timer = new Timer();
    Toast toast;
//    private BasePreferenceHelper prefHelper;

    public SyncService() {

    }

    private void startSyncTimer() {
        SyncSurveyFormsJob.scheduleJob();
//        timer.scheduleAtFixedRate(new syncDataTask(), 0, INTERVAL);
    }

    private void startAlarmManager() {
        Intent myIntent = new Intent(getApplicationContext(), SyncService.class);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 10);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

//        Toast.makeText(getApplicationContext(), "Start Alarm", Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "Service onCreate");
//        prefHelper = new BasePreferenceHelper(getApplicationContext());

        startSyncTimer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        toast = Toast.makeText(getApplicationContext(), "Sync service starts", Toast.LENGTH_SHORT);
        // toast.setText("Sync service starts");
        toast.show();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        startAlarmManager();
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        startAlarmManager();
        super.onTaskRemoved(rootIntent);
    }

//    private class syncDataTask extends TimerTask {
//
//        public void run() {
//            Log.d(TAG, "sync data task executed..");
//            uploadSavedCheckedInDoctors();
//            uploadAddedDoctors();
//            uploadUpdatedDoctors();
//            uploadSavedSurveyForm();
//        }
//
//        public void uploadSavedCheckedInDoctors() {
//            ArrayList<DoctorCheckInItem> checkInDoctors = prefHelper.getDoctorCheckInList();
//            if (checkInDoctors != null
//                    && !checkInDoctors.isEmpty()) {
//
//                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//                String jsonData = gson.toJson(checkInDoctors);
//                Log.d(TAG, jsonData);
//
//                Call<WebResponse<Object>> service = WebServiceFactory.getInstance().checkInDoctors(
//                        Constants.kWebCheckInDoctor, jsonData);
//                service.enqueue(new Callback<WebResponse<Object>>() {
//                    @Override
//                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
//                        if (response != null) {
//                            if (response.body() != null
//                                    && response.body().getHeader().success.equals("1")) {
//                                Intent netIntent = new Intent(Constants.kUIACTION);
//                                netIntent.putExtra(Constants.kACTION, "status");
//                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
//                                Utils.triggerBroadcast(SyncService.this, netIntent);
//                                prefHelper.putMissedCheckinCalls(0);
//                                Intent intent = new Intent(Constants.kUIACTION);
//                                intent.putExtra(Constants.kACTION, "checkinCalls");
//                                intent.putExtra(Constants.kNoOfCheckinCalls, prefHelper.getMissedCheckinCalls());
//                                Utils.triggerBroadcast(SyncService.this, intent);
//
//                                prefHelper.putDoctorCheckInList(new ArrayList<DoctorCheckInItem>());
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
//                        Intent netIntent = new Intent(Constants.kUIACTION);
//                        netIntent.putExtra(Constants.kACTION, "status");
//
//                        if (Utils.isNetworkConnected(SyncService.this)) {
//
//                            if (Utils.isInternetAvailable()) {
//                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
//
//
//                            }
//                        } else {
//                            netIntent.putExtra(Constants.kInternetStatus, "Not Connected");
//
//                        }
//                        Utils.triggerBroadcast(SyncService.this, netIntent);
//                        Log.d(TAG, "Webservice failed: " + t.getLocalizedMessage());
//                        int noOfMissedCheckinCalls = prefHelper.getMissedCheckinCalls();
//                        if (noOfMissedCheckinCalls == -1)
//                            noOfMissedCheckinCalls = 0;
//                        prefHelper.putMissedCheckinCalls(++noOfMissedCheckinCalls);
//                        Intent intent = new Intent(Constants.kUIACTION);
//                        intent.putExtra(Constants.kACTION, "checkinCalls");
//                        intent.putExtra(Constants.kNoOfCheckinCalls, prefHelper.getMissedCheckinCalls());
//                        Utils.triggerBroadcast(SyncService.this, intent);
//                    }
//                });
//            }
//        }
//
//        public void uploadAddedDoctors() {
//
//            ArrayList<Doctor> doctors = prefHelper.getAddedDoctorsList();
//            if (doctors != null
//                    && !doctors.isEmpty()) {
//                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//                String jsonData = gson.toJson(doctors);
//                Log.d(TAG, jsonData);
//
//                Call<WebResponse<Object>> service = WebServiceFactory.getInstance().addDoctors(
//                        Constants.kWebAddDoctor, jsonData);
//                service.enqueue(new Callback<WebResponse<Object>>() {
//                    @Override
//                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
//                        if (response != null) {
//                            if (response.body() != null
//                                    && response.body().getHeader().success.equals("1")) {
//                                Intent netIntent = new Intent(Constants.kUIACTION);
//                                netIntent.putExtra(Constants.kACTION, "status");
//                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
//                                Utils.triggerBroadcast(SyncService.this, netIntent);
//                                prefHelper.putAddDoctorList(new ArrayList<Doctor>());
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
//                        Log.d(TAG, "Webservice failed: " + t.getLocalizedMessage());
//                        Intent netIntent = new Intent(Constants.kUIACTION);
//                        netIntent.putExtra(Constants.kACTION, "status");
//
//                        if (Utils.isNetworkConnected(SyncService.this)) {
//
//                            if (Utils.isInternetAvailable()) {
//                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
//
//
//                            }
//                        } else {
//                            netIntent.putExtra(Constants.kInternetStatus, "Not Connected");
//
//                        }
//                        Utils.triggerBroadcast(SyncService.this, netIntent);
//                    }
//                });
//            }
//        }
//
//        public void uploadUpdatedDoctors() {
//
//            ArrayList<Doctor> doctors = prefHelper.getUpdatedDoctorsList();
//            if (doctors != null
//                    && !doctors.isEmpty()) {
////                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
//                String jsonData = GsonFactory.getConfiguredGson().toJson(doctors);
//                Log.d(TAG, jsonData);
//
//                Call<WebResponse<Object>> service = WebServiceFactory.getInstance().updateDoctors(
//                        Constants.kWebUpdateDoctor, jsonData);
//                service.enqueue(new Callback<WebResponse<Object>>() {
//                    @Override
//                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
//                        if (response != null) {
//                            if (response.body() != null
//                                    && response.body().getHeader().success.equals("1")) {
//                                Intent netIntent = new Intent(Constants.kUIACTION);
//                                netIntent.putExtra(Constants.kACTION, "status");
//                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
//                                Utils.triggerBroadcast(SyncService.this, netIntent);
//                                prefHelper.putUpdateDoctorList(new ArrayList<Doctor>());
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
//                        Log.d(TAG, "Webservice Failed " + t.getLocalizedMessage());
//                        Intent netIntent = new Intent(Constants.kUIACTION);
//                        netIntent.putExtra(Constants.kACTION, "status");
//
//                        if (Utils.isNetworkConnected(SyncService.this)) {
//
//                            if (Utils.isInternetAvailable()) {
//                                netIntent.putExtra(Constants.kInternetStatus, "Connected");
//
//
//                            }
//                        } else {
//                            netIntent.putExtra(Constants.kInternetStatus, "Not Connected");
//                        }
//                        Utils.triggerBroadcast(SyncService.this, netIntent);
//                    }
//                });
//            }
//        }
//    }
//
//    private void uploadSavedSurveyForm() {
//        final JSONArray jsonArray = prefHelper.getSavedSurveryFormCheckInList();
//        Log.e(TAG, "json array from sunc service " + jsonArray);
//        if (jsonArray != null && jsonArray.length() > 0) {
//            Call<WebResponse<Object>> call = WebServiceFactory.getInstance().checkin(Constants.CHECK_IN, jsonArray);
//            call.enqueue(new Callback<WebResponse<Object>>() {
//                @Override
//                public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
//                    if (response.body() == null)
//                        Log.e(TAG, "Null");
//                    else if (response.body().isSuccess()) {
//
//                        prefHelper.removeCheckInFormPreference();
//                        Log.e(TAG, "Suvrey Form Size After Clear " + jsonArray.length());
//                        Log.e(TAG, "onSuccess " + response.body().getMessage());
//                        Intent netIntent = new Intent(Constants.kUIACTION);
//                        netIntent.putExtra(Constants.kACTION, "status");
//                        netIntent.putExtra(Constants.kInternetStatus, "Connected");
//                        Utils.triggerBroadcast(SyncService.this, netIntent);
//                        prefHelper.putMissedCheckinCalls(0);
//                        Intent intent = new Intent(Constants.kUIACTION);
//                        intent.putExtra(Constants.kACTION, "checkinCalls");
//                        intent.putExtra(Constants.kNoOfCheckinCalls, prefHelper.getMissedCheckinCalls());
//                        Utils.triggerBroadcast(SyncService.this, intent);
//
//
//                    } else {
//                        Log.e(TAG, "onError " + response.body().getMessage());
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
//                    Log.e(TAG, "" + t.getMessage());
//                    Intent netIntent = new Intent(Constants.kUIACTION);
//                    netIntent.putExtra(Constants.kACTION, "status");
//
//                    if (Utils.isNetworkConnected(SyncService.this)) {
//
//                        if (Utils.isInternetAvailable()) {
//                            netIntent.putExtra(Constants.kInternetStatus, "Connected");
//
//
//                        }
//                    } else {
//                        netIntent.putExtra(Constants.kInternetStatus, "Not Connected");
//
//                    }
//                    Utils.triggerBroadcast(SyncService.this, netIntent);
//                    Log.d(TAG, "Webservice failed: " + t.getLocalizedMessage());
//                    int noOfMissedCheckinCalls = prefHelper.getMissedCheckinCalls();
//                    if (noOfMissedCheckinCalls == -1)
//                        noOfMissedCheckinCalls = 0;
//                    prefHelper.putMissedCheckinCalls(++noOfMissedCheckinCalls);
//                    Intent intent = new Intent(Constants.kUIACTION);
//                    intent.putExtra(Constants.kACTION, "checkinCalls");
//                    intent.putExtra(Constants.kNoOfCheckinCalls, prefHelper.getMissedCheckinCalls());
//                    Utils.triggerBroadcast(SyncService.this, intent);
//
//
//                }
//            });
//        } else Log.e(TAG, "JsonArray is Null");
//    }
}
