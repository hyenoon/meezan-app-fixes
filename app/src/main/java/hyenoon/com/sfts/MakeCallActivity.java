package hyenoon.com.sfts;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.ContactModel;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;

public class MakeCallActivity extends BaseActivity {

    private static final String TAG = MakeCallActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private SearchView searchView;
    private ContactsAdapter mAdapter;
    TextView tv_OpenSetting;
    private static final int REQUEST_READ_CONTACTS = 111;

    private final int CALL_REQUEST = 222;

    ProgressDialog dialog;
    private Cursor cursor;

    private String name, phonenumber;

    private ContactModel mContactModel;

    private int selectedIndex;
    protected BasePreferenceHelper prefHelper;
    private ArrayList<ContactModel> contactModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_call);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tv_OpenSetting = (TextView) findViewById(R.id.tv_Setting);
        prefHelper = new BasePreferenceHelper(this);
    }

    public void callNumber(String PhoneNumber) {

        String currentTime = String.valueOf(Calendar.getInstance().getTimeInMillis());
        Log.e(TAG, "Time " + currentTime);
        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().callNumber(Constants.CALL_NUMBER, prefHelper.getUser().getUserId(), PhoneNumber, currentTime);
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body() == null)
                    Log.e(TAG, "Found null response in Web Services");
                else
                    Log.e(TAG, response.body().getMessage());

            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {
                Log.e(TAG, throwable.getMessage());
            }
        });
    }

    public void GetContactsIntoArrayList() {

        contactModelList = new ArrayList<ContactModel>();

        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor.getCount() > 0) {
            Log.e(TAG, "GetContactsIntoArrayList Called");
            while (cursor.moveToNext()) {

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    mContactModel = new ContactModel();

                    mContactModel.setContactName(name);


                    Cursor phoneCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id},
                            null);
                    if (phoneCursor.moveToNext()) {
                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        mContactModel.setContactNumber(phoneNumber);
                    }

                    phoneCursor.close();

                    Cursor emailCursor = contentResolver.query(
                            ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (emailCursor.moveToNext()) {
                        String emailId = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    }
                    contactModelList.add(mContactModel);
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                if (query.isEmpty()) {
                    mAdapter = new ContactsAdapter(MakeCallActivity.this, contactModelList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(mLayoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(mAdapter);

                } else if (contactModelList != null && !(contactModelList.size() > 0)) {
                    Toast.makeText(MakeCallActivity.this, "Contact Not Found", Toast.LENGTH_SHORT).show();
                } else
                    mAdapter.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }


    private class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.VH> {

        Context ctx;
        ArrayList<ContactModel> contactModelList;

        public ContactsAdapter(Context context, ArrayList<ContactModel> contactModelList) {

            this.ctx = context;
            this.contactModelList = contactModelList;


        }

        @Override
        public VH onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recylerview_makecall, parent, false);
            return new VH(v);
        }

        @Override
        public void onBindViewHolder(VH holder, final int position) {

            holder.tv_Name.setText(contactModelList.get(position).getContactName());
            holder.tv_Number.setText(contactModelList.get(position).getContactNumber());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    callNumber(contactModelList.get(position).getContactNumber());
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ActivityCompat.checkSelfPermission(ctx, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) ctx, new String[]{android.Manifest.permission.CALL_PHONE}, CALL_REQUEST);

                                return;
                            }
                        }

                        Intent callIntent = new Intent(Intent.ACTION_CALL);

                        callIntent.setData(Uri.parse("tel:" + contactModelList.get(position).getContactNumber()));

                        startActivity(callIntent);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return contactModelList.size();
        }

        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                    } else {
                        ArrayList<ContactModel> filteredList = new ArrayList<>();
                        for (ContactModel row : contactModelList) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getContactName().toLowerCase().contains(charString.toLowerCase()) || row.getContactNumber().contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        contactModelList = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = contactModelList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    contactModelList = (ArrayList<ContactModel>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }

        public class VH extends RecyclerView.ViewHolder {
            TextView tv_Name, tv_Number;

            public VH(View itemView) {
                super(itemView);
                tv_Name = (TextView) itemView.findViewById(R.id.tv_name);
                tv_Number = (TextView) itemView.findViewById(R.id.tv_number);

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            searchView.clearFocus();

        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");

    }

    public class backgroundTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            Log.e(TAG, "doInBackground Called");
            GetContactsIntoArrayList();
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = Utils.showSweetLoader(MakeCallActivity.this, SweetAlertDialog.PROGRESS_TYPE, "Fetching Contacts");


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Utils.hideSweetLoader(pDialog);
            updateView();

        }
    }

    private void updateView() {
        mAdapter = new ContactsAdapter(MakeCallActivity.this, contactModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }
}
