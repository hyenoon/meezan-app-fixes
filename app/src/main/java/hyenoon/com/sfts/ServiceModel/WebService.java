package hyenoon.com.sfts.ServiceModel;

import com.google.gson.JsonArray;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import hyenoon.com.sfts.Entities.AreaCodesWrapper;
import hyenoon.com.sfts.Entities.BodyLOV;
import hyenoon.com.sfts.Entities.CheckInModule;
import hyenoon.com.sfts.Entities.Customer;
import hyenoon.com.sfts.Entities.CustomerBody;
import hyenoon.com.sfts.Entities.NotificationBody;
import hyenoon.com.sfts.Entities.SheetBody;
import hyenoon.com.sfts.Entities.SpecialityWrapper;
import hyenoon.com.sfts.Entities.SurveyWrapper;
import hyenoon.com.sfts.Entities.TeamCustomers;
import hyenoon.com.sfts.Entities.TeamCustomersBody;
import hyenoon.com.sfts.Entities.TerritoryDoctor;
import hyenoon.com.sfts.Entities.UserWrapper;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface WebService {

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<UserWrapper>> loginUser(@Query("action") String actionType,
                                             @Query("username") String email,
                                             @Query("password") String password);

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<TerritoryDoctor>>       getDoctors(@Query("action") String actionType,
                                                  @Query("userid") String userid);


    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<SurveyWrapper>> getSurvey(@Query("action") String actionType);

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<SpecialityWrapper>> getSpecialities(@Query("action") String actionType);

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<AreaCodesWrapper>> getAreaCodes(@Query("action") String actionType,
                                                     @Query("user_id") String userid);

    @FormUrlEncoded
    @POST("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> updateUserLocation(@Field("action") String actionType,
                                                 @Field("users_data") String user_data);

    @FormUrlEncoded
    @POST("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> checkInDoctors(@Field("action") String actionType,
                                             @Field("checkin_info") String data);

    @FormUrlEncoded
    @POST("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> addDoctors(@Field("action") String actionType,
                                         @Field("doctors_data") String data);

    @FormUrlEncoded
    @POST("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> updateDoctors(@Field("action") String actionType,
                                            @Field("doctors_data") String data);

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> getVersionCheck(@Query("action") String actionType,
                                              @Query("version_no") String versionNo);

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> startShiftTime(@Query("action") String actionType,
                                             @Query("user_id") String versionNo,
                                             @Query("start_time") Long startTime
    );

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> endShiftTime(@Query("action") String actionType,
                                           @Query("user_id") String versionNo,
                                           @Query("end_time") Long startTime
    );


    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> changeTime(@Query("action") String actionType,
                                         @Query("user_id") String userID,
                                         @Query("new_time") String newTime
    );

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> callNumber(@Query("action") String actionType,
                                         @Query("user_id") String userID,
                                         @Query("phone_number") String phoneNumber,
                                         @Query("call_time") String time
    );

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<CustomerBody>> getNearByCustomers(@Query("action") String actionType,
                                                       @Query("userid") String userID,
                                                       @Query("latitude") String latitude,
                                                       @Query("longitude") String longitude
    );

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<TeamCustomersBody>> getTeamCustomers(@Query("action") String actionType,
                                                          @Query("userid") String userid);

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> updateFCMToken(@Query("action") String action,
                                             @Query("user_id") String userId,
                                             @Query("android_push_id") String pushId
    );

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<SheetBody>> getPerfomanceSheet(@Query("action") String action,
                                                    @Query("userid") String userId
    );


    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<BodyLOV>> getLOV(@Query("action") String action
    );

    @GET("mobileservice/mobileservice.php")
    Call<WebResponse<Object>> checkin(@Query("action") String action,
                                      @Query("checkin_info") JSONArray checkinInfo


    );

}
