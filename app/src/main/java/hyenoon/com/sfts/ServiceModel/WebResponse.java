package hyenoon.com.sfts.ServiceModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hyenoon.com.sfts.Entities.CustomerBody;

public class WebResponse<T> {

	private T body;
	
	@SerializedName("header")
	@Expose
	public Header header;
	
	public String message;
	
	public Header getHeader() {
		return header;
	}
	
	public void setResult( Header header ) {
		this.header = header;
	}

	public String getMessage() {
		return header.message;
	}
	
	public T getBody() {
		return body;
	}
	
	public void setBody( T body ) {
		this.body = body;
	}
	
	public boolean isSuccess() {
		return header.success.equalsIgnoreCase("1");
	}

	public class Header {

		@SerializedName("success")
		@Expose
		public String success;
		@SerializedName("message")
		@Expose
		public String message;

		}
}
