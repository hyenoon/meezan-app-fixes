package hyenoon.com.sfts.ServiceModel;

import hyenoon.com.sfts.Constants.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by macbookpro on 08/01/16.
 */
public class WebServiceFactory {

    private static WebService instance;

    public static WebService getInstance() {
        if (instance == null) {
//            OkHttpClient name = new OkHttpClient();
//            name.setReadTimeout(25, TimeUnit.SECONDS);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            instance = retrofit.create(WebService.class);
        }
        return instance;
    }
}
