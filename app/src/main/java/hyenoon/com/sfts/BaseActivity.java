package hyenoon.com.sfts;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.Location.SimpleLocation;


import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by dev.akber on 6/17/2016.
 */
public class BaseActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;

    private static String TAG = "BaseActivity";
    protected SweetAlertDialog pDialog;
    protected BasePreferenceHelper prefHelper;
    public Context mContext;
    private SimpleLocation mLocation;
    public static String userID;

    public static boolean checkPermission(String strPermission, Context context) {
        int result = ContextCompat.checkSelfPermission(context, strPermission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void startServiceClass(Class<?> serviceClass) {
        Log.d(TAG, "Starts: " + serviceClass.getName());
        if (!checkServiceRunning(serviceClass)) {
            Intent serviceIntent = new Intent(this, serviceClass);
            this.startService(serviceIntent);
        }
    }

    public void stopServiceClass(Class<?> serviceClass) {
        Log.d(TAG, "Stops: " + serviceClass.getName());
        if (checkServiceRunning(serviceClass))
            this.stopService(new Intent(this, serviceClass));
    }

    public boolean checkServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager
                .getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }

    protected void initLocationManager() {

        mLocation = SimpleLocation.getInstance(this);
        if (!mLocation.hasLocationEnabled()) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Please turn on GPS in settings");
            dialog.setTitle("Warning");

            dialog.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SimpleLocation.openSettings(mContext);
                    dialog.dismiss();
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        SharedPreferences pref = getSharedPreferences("preferences",MODE_PRIVATE);
        Log.e(TAG,"Preference size "+pref.getAll().size());
        prefHelper = new BasePreferenceHelper(this);

//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
