package hyenoon.com.sfts.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by dev.akber on 1/24/2017.
 */

public class BootReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startActivity(context.getPackageManager().getLaunchIntentForPackage(context.getPackageName()));

    }
}
