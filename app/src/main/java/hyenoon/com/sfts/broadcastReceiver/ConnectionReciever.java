package hyenoon.com.sfts.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by us on 11/7/2017.
 */

public class ConnectionReciever extends BroadcastReceiver {
    private static final String TAG = ConnectionReciever.class.getSimpleName();
    BasePreferenceHelper prefHelper;

    @Override
    public void onReceive(final Context context, Intent intent) {


        Log.e("ConnectionReciever", "Network Status " + Utils.isNetworkConnected(context));
        Log.e("ConnectionReciever", "Internet Status " + Utils.isInternetAvailable());

        if (Utils.isNetworkConnected(context)) {

            if (Utils.isInternetAvailable()) {

                prefHelper = new BasePreferenceHelper(context);
                UploadSavedChechkInFormList(context);

                final SharedPreferences preferences = context.getSharedPreferences(Constants.startShift, context.MODE_PRIVATE);
                String shift = preferences.getString("shift", "");
                Long startTime = preferences.getLong("time", 0);
                if (shift.equalsIgnoreCase("Start Shift")) {


                    Call<WebResponse<Object>> call = WebServiceFactory.getInstance().startShiftTime(Constants.startShift,
                            prefHelper.getUser().getUserId(), startTime);
                    call.enqueue(new Callback<WebResponse<Object>>() {
                        @Override
                        public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                            if (response.body().isSuccess()) {
                                Log.e("ConnectionReciever", response.body().getMessage());
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                            } else if (!response.body().isSuccess()) {
                                Utils.showShortToastInCenter(context, response.body().getHeader().message);
                            }
                        }

                        @Override
                        public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {
                            Log.e("ConnectionReciever", "Internet is not available");
                        }
                    });


                } else {


                    Call<WebResponse<Object>> call = WebServiceFactory.getInstance().endShiftTime(Constants.endShift,
                            prefHelper.getUser().getUserId(), startTime);
                    call.enqueue(new Callback<WebResponse<Object>>() {
                        @Override
                        public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                            if (response.body().isSuccess()) {
                                Log.e("ConnectionReciever", response.body().getMessage());
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                            } else if (!response.body().isSuccess()) {
                                Utils.showShortToastInCenter(context, response.body().getHeader().message);
                            }
                        }

                        @Override
                        public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {
                            Log.e("ConnectionReciever", "Internet is not available");
                        }
                    });


                }

            }
        } else {
//            Utils.showShortToastInCenter(context, "Internet is not available");
            Log.e(TAG, "Internet is not available");
        }


    }

    private void UploadSavedChechkInFormList(final Context context) {

        JSONArray jsonArray = prefHelper.getSavedSurveryFormCheckInList();
        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().checkin(
                Constants.CHECK_IN,
                jsonArray);
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body() == null)
                    Log.e("ConnectionReciever", "Null");
                else if (response.body().isSuccess()) {

                    SharedPreferences settings = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
                    settings.edit().remove(Constants.KEY_SAVED_CHECK_IN_DOCTOR_LIST).apply();
                    Log.e("ConnectionReciever", "onSuccess " + response.body().getMessage());


                } else {
                    Log.e("ConnectionReciever", "onError " + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                Log.e("ConnectionReciever", "" + t.getMessage());
            }
        });
    }
}
