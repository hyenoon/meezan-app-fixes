package hyenoon.com.sfts.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import java.util.Calendar;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.HelperClasses.BasePreferenceHelper;
import hyenoon.com.sfts.HelperClasses.PreferenceHelper;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebService;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import hyenoon.com.sfts.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by us on 11/9/2017.
 */

public class TimeChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        final String TAG = "TimeChangedReceiver";
        Log.e("TimeChangedReceiver", String.valueOf(Calendar.getInstance().getTimeInMillis()));
        BasePreferenceHelper prefHelper = new BasePreferenceHelper(context);

        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().changeTime(Constants.CHANGE_TIME,prefHelper.getUser().getUserId(), String.valueOf(Calendar.getInstance().getTimeInMillis()));
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body() == null)
                    Log.e(TAG, "found null in web response");
                else if (response.body().isSuccess())
                    Log.e(TAG, response.body().getMessage());
                else if (!response.body().isSuccess())
                    Log.e(TAG, response.body().getMessage());
                else
                    Log.e(TAG, "Something went wrong,please try again");
                Log.e(TAG, response.body().getMessage());
            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {
                Log.e(TAG, throwable.getMessage());
            }
        });


    }
}
