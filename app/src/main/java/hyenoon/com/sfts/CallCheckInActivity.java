package hyenoon.com.sfts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import hyenoon.com.sfts.Constants.Constants;
import hyenoon.com.sfts.Entities.TeamCustomers;
import hyenoon.com.sfts.ServiceModel.WebResponse;
import hyenoon.com.sfts.ServiceModel.WebServiceFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallCheckInActivity extends BaseActivity {

    EditText et_Number;
    String number;
    private String TAG = getClass().getName();
    private final int CALL_REQUEST = 222;
    Context context;
    Button btn_CalChehckIn;
    ArrayList<TeamCustomers> teamCustomersArrayList = new ArrayList<>();
    private boolean isNumberFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cold_call);
        context = this;
        init();
        setView();

    }

    private void setView() {
        //Retrun true if User Type is BDO
        if (validateUsertype()) {
            setToolBar("Cold Call");
        } else {
            setToolBar("Check in By Phone");
        }
    }

    private boolean validateUsertype() {
        if (prefHelper.getUser().getUserType().equals(Constants.BDO)) {
            return true;
        } else {
            btn_CalChehckIn.setText("Check In");
            return false;
        }
    }

    private void setToolBar(String s) {

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(s);
        }
    }

    private void init() {
        et_Number = (EditText) findViewById(R.id.et_coldcallactivity_number);
        btn_CalChehckIn = (Button) findViewById(R.id.btn_callcheckinActivity_callchechkin);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickMakeCal(View view) {

        number = et_Number.getText().toString();
        if (number.length() < 11) {
            et_Number.setError("incomplete  number");

        } else {
            //Retrun true if User Type is BDO
            if (!validateUsertype()) {

                Intent surveyForm = new Intent(CallCheckInActivity.this, CheckInFormActivity.class);
                surveyForm.putExtra("number", number);
                surveyForm.putExtra("type", "checkinbyphonenumber");
                startActivity(surveyForm);

            } else {
                callDailer(number);
            }
        }

    }

    private void callDailer(String number) {
        // OPEN SURVEY FORM..

        Intent surveyForm = new Intent(CallCheckInActivity.this, CheckInFormActivity.class);

        surveyForm.putExtra("number", number);
        surveyForm.putExtra("type", "coldcall");

        startActivity(surveyForm);

        // callAPI(number);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, CALL_REQUEST);

                    return;
                }
            }

            Intent callIntent = new Intent(Intent.ACTION_CALL);

            callIntent.setData(Uri.parse("tel:" + number));

            startActivity(callIntent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /*public void callAPI(String PhoneNumber) {

        String currentTime = String.valueOf(Calendar.getInstance().getTimeInMillis());
        Log.e(TAG, "Time " + currentTime);
        Call<WebResponse<Object>> call = WebServiceFactory.getInstance().callNumber(Constants.CALL_NUMBER, prefHelper.getUser().getUserId(), PhoneNumber, currentTime);
        call.enqueue(new Callback<WebResponse<Object>>() {
            @Override
            public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                if (response.body() == null)
                    Log.e(TAG, "Found null response in Web Services");
                else
                    Log.e(TAG, response.body().getMessage());

            }

            @Override
            public void onFailure(Call<WebResponse<Object>> call, Throwable throwable) {
                Log.e(TAG, throwable.getMessage());
            }
        });
    }
*/
}
