package hyenoon.com.sfts.Constants;

/**
 * Created by dev.akber on 6/17/2016.
 */
public class Constants {
    public static final String CHANGE_TIME = "time_change";
    public static final String CALL_NUMBER ="voice_call_register";
    public static final String GET_NEARBY_CUSTOMER = "get_nearby_customers";
    public static final String GET_PERFORMANCE_SHEET= "get_performance_sheet";
    public static final String GET_LOV ="get_lov";
    public static final String CHECK_IN ="check_in";
    public static String SERVER_URL = "http://meezan.uhfsolutions.com/";
    public static final String PREF_NAME = "keyPref";
    public static final String LOGIN_KEY = "THE_Login_Key";
    public static final String KEY_SAVED_CHECK_IN_FORM_LIST = "saved_check_in_form_list";
    public static String kWebLoginUser  = "login";
    public static String kWebGetDoctors = "get_my_customers";
    public static String kWebGetTeamCustomers = "get_my_team_customers";
    public static String kNullRespone="something went wrong, pleas4e try again";
    public static final String FCM_ID = "fcm_registration_id_key";
    public static final String REGISTRATION_COMPLETE = "registration_complete_key";
    public static final String ACTION_UPDATE_GCM = "fcm_refresh";
    public static final String INTENT_NOTIFICATION = "gcm_notification_key";
    public static final String NOTIFICATION_KEY = "notification_key";
    public static String NOTIFICATION_COUNTER_RECEIVER = "NotificationCounter";
    public static String kWebGetSurvey = "get_survey";
    public static String kWebGetSpecialities = "get_speciality";
    public static String kWebGetAreaCodes = "get_areas";
    public static String kWebUpdateLocation = "update_user_location";
    public static String kWebCheckInDoctor = "check_in";
    public static String kWebAddDoctor = "add_doctor";
    public static String kWebUpdateDoctor = "update_doctor";
    public static String kWebCheckVersion = "version_check";
    public static String kUIACTION = "missed_sync_calls";
    public static String kACTION = "action";
    public static String kLAT = "lat";
    public static String kLONG = "long";
    public static String kLocationZero = "locationZero";
    public static String kNoOfTrackingCalls = "noOfTrackingCalls";
    public static String kNoOfCheckinCalls = "noOfCheckinCalls";
    public static String kInternetStatus = "status";

    public static final String KEY_SAVED_CHECK_IN_DOCTOR_LIST = "saved_check_in_doctor_list";
    public static String startShift="start_shift";
    public static String endShift="end_shift";
    public static String ShiftKey="startShift";

    public static String BDO="32";


}

