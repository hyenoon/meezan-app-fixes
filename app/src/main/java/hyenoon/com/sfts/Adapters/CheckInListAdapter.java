package hyenoon.com.sfts.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import hyenoon.com.sfts.Entities.SurveyQuestionItem;
import hyenoon.com.sfts.Entities.SurveyProductItem;
import hyenoon.com.sfts.Entities.Product;
import hyenoon.com.sfts.Entities.Question;
import hyenoon.com.sfts.Entities.Survey;
import hyenoon.com.sfts.R;
import hyenoon.com.sfts.Utils.Utils;

/**
 * Created by dev.akber on 7/12/2016.
 */
public class CheckInListAdapter extends BaseAdapter implements RadioGroup.OnCheckedChangeListener {

    Context context;
    Survey survey;
    private ArrayList mData = new ArrayList();
    private ArrayList mQuestionData = new ArrayList();
    private static LayoutInflater inflater = null;

    public CheckInListAdapter(Context context) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CheckInListAdapter(Context context, Survey survey) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.survey = survey;
        for (Question question:
             survey.getQuestions()) {
            SurveyQuestionItem _obj = new SurveyQuestionItem();
            _obj.setQuestion(question);
            _obj.setQuestionId(question.getId());
            _obj.setAnswer("No");
            _obj.setQuantity("0");

            mQuestionData.add(_obj);
        }

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ArrayList getProducts() {
        return mData;
    }

    public ArrayList getQuestions() {
        return mQuestionData;
    }

    public void addItem(final SurveyProductItem item) {
        mData.add(item);
        notifyDataSetChanged();
    }

    public void showQuantityDialog(final int position) {
        // custom dialog
        final Dialog dialog = new Dialog(this.context);
        dialog.setContentView(R.layout.popup);
        dialog.setTitle("Please enter Quantity");

        SurveyQuestionItem objQuestion = (SurveyQuestionItem) mQuestionData.get(position);

        // set the custom dialog components - text, image and button
        final EditText text = (EditText) dialog.findViewById(R.id.edtxtQuantity);
        text.setText(objQuestion.getQuantity());

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!Utils.isEmptyOrNull(text.getText().toString())) {
                    SurveyQuestionItem _objQuestion = (SurveyQuestionItem) mQuestionData.get(position);
                    _objQuestion.setQuantity(text.getText().toString());
                    mQuestionData.set(position, _objQuestion);

                    dialog.dismiss();
                }
                else {
                    text.setError("Field can't be empty");
                }
            }
        });

        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialogButtonCancel);
        // if button is clicked, close the custom dialog
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void initRemoveButton(int position, View view) {
        final Button btnRemove = (Button) view.findViewById(R.id.btnRemoveProductItem);
        btnRemove.setTag(position);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int _position = (int) v.getTag();
                mData.remove(_position);
                notifyDataSetChanged();
            }
        });
    }

    public void initSpinners(int productIndex, View view) {

        final SurveyProductItem surveyProductItem = (SurveyProductItem) mData.get(productIndex);

        final Spinner spinnerOtherProducts = (Spinner)view.findViewById(R.id.otherProducts);
        spinnerOtherProducts.setTag(productIndex);

        final Spinner spinnerProducts = (Spinner)view.findViewById(R.id.ourProducts);
        spinnerProducts.setTag(productIndex);

        ArrayAdapter<Product> adapterProduct = new ArrayAdapter<Product>(context,
                android.R.layout.simple_spinner_item,
                survey.getProducts());
        adapterProduct.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProducts.setAdapter(adapterProduct);
        spinnerProducts.setSelection(getIndex(spinnerProducts, surveyProductItem.getSelectedProduct()));

        spinnerProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Product product = survey.getProducts().get(position);
                int _position = (int) spinnerProducts.getTag();

                SurveyProductItem _checkInProduct = (SurveyProductItem) mData.get(_position);
                _checkInProduct.setSelectedProduct(product.getName());
                mData.set(_position, _checkInProduct);

                ArrayAdapter<String> adapterOtherProduct = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_dropdown_item,
                        product.getCompetitiveProduct());
                adapterOtherProduct.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerOtherProducts.setAdapter(adapterOtherProduct);

                spinnerOtherProducts.setSelection(getIndex(spinnerOtherProducts, surveyProductItem.getSelectedcompetitiveProduct()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerOtherProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int _position = (int) spinnerOtherProducts.getTag();

                SurveyProductItem _checkInProduct = (SurveyProductItem) mData.get(_position);
                _checkInProduct.setSelectedcompetitiveProduct(parent.getItemAtPosition(position).toString());
                mData.set(_position, _checkInProduct);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    private ArrayList <RadioButton> getListOfRadioButtons(RadioGroup radioGroup) {

        int count = radioGroup.getChildCount();
        ArrayList<RadioButton> listOfRadioButtons = new ArrayList<RadioButton>();
        for (int i=0;i<count;i++) {
            View o = radioGroup.getChildAt(i);
            if (o instanceof RadioButton) {
                listOfRadioButtons.add((RadioButton)o);
            }
        }

        return listOfRadioButtons;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return (mQuestionData.size() + mData.size());
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (position < mQuestionData.size())
            return mQuestionData.get(position);
        else
            return mData.get(position - mQuestionData.size());
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return  (position < mQuestionData.size()) ? 0 : 1;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        int type = getItemViewType(position);
        if (vi == null) {
            if (type == 0)
                vi = inflater.inflate(R.layout.survey_item, null);
            else
                vi = inflater.inflate(R.layout.survey_item_add_product, null);
        }

        if (type == 0) {
            TextView textView = (TextView)vi.findViewById(R.id.txt_survey_item_question);
            RadioGroup radioGroup = (RadioGroup)vi.findViewById(R.id.radioGroup);
            radioGroup.setTag(position);
            radioGroup.setOnCheckedChangeListener(this);

            SurveyQuestionItem _objQuestion = (SurveyQuestionItem) mQuestionData.get(position);
            textView.setText(_objQuestion.getQuestion().getQuestion());

            ArrayList<RadioButton> radioButtons = getListOfRadioButtons(radioGroup);
            if (_objQuestion.getAnswer().equalsIgnoreCase("Yes")) {
                RadioButton radioButton = radioButtons.get(0);
                radioButton.setChecked(true);
            }
            else {
                RadioButton radioButton = radioButtons.get(1);
                radioButton.setChecked(true);
            }
        }
        else {
            int productIndex = position - mQuestionData.size();

            initRemoveButton(productIndex, vi);
            initSpinners(productIndex, vi);
        }

        return vi;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        int position = (int)group.getTag();
        SurveyQuestionItem _objQuestion = (SurveyQuestionItem) mQuestionData.get(position);

        switch (checkedId) {
            case R.id.radio_survey_item_yes:
                if (_objQuestion.getQuestion().getQty().equalsIgnoreCase("yes"))
                    showQuantityDialog(position);

                _objQuestion.setAnswer("Yes");
                mQuestionData.set(position, _objQuestion);
                break;
            case R.id.radio_survey_item_no:
                _objQuestion.setAnswer("No");
                mQuestionData.set(position, _objQuestion);
                break;
        }
    }
}
