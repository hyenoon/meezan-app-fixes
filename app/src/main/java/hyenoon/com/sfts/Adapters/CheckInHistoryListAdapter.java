package hyenoon.com.sfts.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hyenoon.com.sfts.Entities.DoctorCheckInItem;
import hyenoon.com.sfts.R;

/**
 * Created by dev.akber on 7/21/2016.
 */
public class CheckInHistoryListAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<DoctorCheckInItem> listCheckedInDoctors;
    private LayoutInflater layoutInflater;

    public CheckInHistoryListAdapter(Context context, ArrayList<DoctorCheckInItem> list) {
        this.context = context;
        this.listCheckedInDoctors = list;
    }

    @Override
    public int getCount() {
        return this.listCheckedInDoctors.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listCheckedInDoctors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null)
            convertView = layoutInflater.inflate(R.layout.recycler_item_doctor_list, parent, false);

        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtSpeciality = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);

        DoctorCheckInItem item = listCheckedInDoctors.get(position);
        txtTitle.setText(item.getDoctor().getCustomerName());
        txtSpeciality.setText(item.getDoctor().getContactNumber());
        txtDesc.setText(item.getDoctor().getAddress());

        return convertView;
    }
}
